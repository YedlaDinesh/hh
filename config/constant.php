<?php
// app loaded for mobile and wifi/hotspot
return [
	'APP_LOADED' => ['1' => 'Provisioned', '2' => 'Active', '3' => 'Lost', '4'=> 'Inactive'],

//Class type for sim	
	'SIM_TYPE' => ['1' => 'Class 1', '2' => 'Class 2'],

//Sim status
	'SIM_STATUS' => ['1' => 'Yes', '2' => 'No'],

//Faq Category type	
'FAQ_CATEGORY' => ['1' => 'General questions', '2' => 'My pocket wifi' , '3' => 'Delivery and return', '4' => 'Network speed and data usage', '5' => 'Technical issues', '6' => 'Payment'],


//Device Type
'DEVICE_TYPE' => ['1' => 'Mobile', '2' => 'Wifi Hotspot'],

'IS_PUBLISHED' => ['1' => 'Published', '2' => 'Not Published'],

//Order status
"ORDERS_STATUS" => ['1' => 'Provisioned', '2' => 'No']
];
