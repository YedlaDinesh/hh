@extends('layouts.home')

@section('content')
<!--banner-section-->

<section class="blog-page-main block-element">
    <div class="container">
        <div class="row">
            <div class="col-sm-9">
                <div class="blog-content">
                @if(!$allblogs->isEmpty())
                    @foreach ($allblogs as $key => $blog_post)
                    <div class="blog-content-inner">
                        <img src="{{ URL::to('/').'/images/blogs/'.$blog_post->image }}" alt="">
                        <a href="{{ url('blog/'.$blog_post->slug) }}"> <span>{{$blog_post->title}}</span></a>
                        @php
                        $blog_description = Str::of($blog_post->description)->words(37, '...')
                        @endphp
                        <div class="content">{!! $blog_description !!}</div>

                        <a href="{{ url('blog/'.$blog_post->slug) }}" class="btn btn-primary ">Read More</a>
                    </div>
                    @endforeach
                @else
                    <div class="blog-content-inner">
                        <p>No blogs found</p>
                    </div>
                @endif
                </div>
            </div>

            <div class="col-sm-3">
                <div class="blog-content-list">
                    <div class="recen-post">
                        <span>Recent Posts</span>
                        <ul>
                            @foreach ($recent_post as $key => $recent)
                            <li>
                                <a href="{{ url('blog/'.$recent->slug) }}">{{ $recent->title }}</a>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="blog-clouds">
                        <span>Blog Cloud</span>
                       
                            @foreach ($categories as $key => $categorie)
                       
                                <a href="{{ url('blogs?category='.$categorie->id) }}">{{$categorie->category_name}}</a>
                       
                            @endforeach
                       
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                {!! $allblogs->links() !!}
            </div>
        </div>
    </div>
</section>

@endsection