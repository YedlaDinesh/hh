@extends('layouts.home')

@section('content')
<!--banner-section-->

<section class="login-pahe-main book-now procees-page block-element">
    <div class="container">
        <div class="row">
            <div class="col-sm-8">
                <div class="login-lft">

                    <div class="login-rgt">
                        <div class="partner-form">
                            <form role="form" method="post" action="" id="waitform" enctype="multipart/form-data">
                                <input type="hidden" value="{{csrf_token()}}" name="_token" />
                                <input type="hidden" value="{{request()->id}}" name="id" />

                                <div class="inner-fom">
                                    <div class="form-group">
                                        <label>Full Name</label>
                                        <span><input type="text" name="full_name" id="full_name"
                                                placeholder="ss@gmail.com" value=""></span>
                                    </div>
                                    <div class="form-group">
                                        <label>Father Name</label>
                                        <span><input type="text" name="father" id="father" placeholder="AA01H0001"
                                                value=""></span>
                                    </div>
                                    <div class="form-group">
                                        <label>Residential Address </label>
                                        <span><input type="text" name="address" id="address" placeholder="AA01H0001"
                                                value=""></span>
                                    </div>
                                    <div class="form-group street">
                                        <label> </label>
                                        <span>
                                            <div class="clander"><label>Street</label><input type="text" name="street"
                                                    id="street" placeholder="FEQDFF" value=""></div>
                                        </span>
                                    </div>
                                    <div class="form-group">
                                        <label> </label>
                                        <span>
                                            <div class="clander"><label>Postal Code</label><input type="text"
                                                    name="pincode" id="pincode" placeholder="DWEWDF" value=""></div>
                                            <div class="clander"><label>City</label><input name="city" id="city"
                                                    type="text" placeholder="SDDWSDDD" value=""></div>
                                        </span>
                                    </div>

                                    <div class="form-group full-row">
                                        <label>Country</label>
                                        <span>
                                            <div class="clander"><select name="country" id="country">
                                                    <option value="volvo">India</option>
                                                    <option value="saab">Saab</option>
                                                    <option value="mercedes">Mercedes</option>
                                                    <option value="audi">Audi</option>
                                                </select></div>
                                        </span>
                                    </div>
                                    <div class="form-group accesory">
                                        <label>Phone Number</label>
                                        <span>
                                            <div class="clander"><input type="tel" placeholder="+01-1234567890
					" id="phone" name="phone"></div>
                                        </span>
                                    </div>
                                    <div class="form-group full-row">

                                        <div class="form-group">
                                            <label></label>
                                            <input class="bl-btn" type="submit" id="btnupload" value="Upload" />
                                        </div>
                                    </div>
                                </div>

                        </div>

                    </div>

                </div>
            </div>
            <div class="col-sm-4">
                <div class="payment-bills">
                    <div class="total-payment upload-document">
                        <ul>
                            <li>Upload Scan Copy of E-Visa <span>(For Travel phone Booking)</span><img
                                    src="{{ asset('/images/upload.png') }}" alt=""><input type="file" name="doc1"
                                    class="form-control"></li>
                            <li>Upload Scan Copy of E-Visa <span>(For Travel phone Booking)</span><img
                                    src="{{ asset('/images/upload.png') }}" alt=""><input type="file" name="doc2"
                                    class="form-control"></li>
                            <li>Upload Scan Copy of Passport <img src="{{ asset('/images/upload.png') }}" alt=""><input
                                    type="file" name="doc3" class="form-control"> </li>
                        </ul>
                    </div>
                </div>
            </div>
            </form>
        </div>
    </div>
</section>
@endsection