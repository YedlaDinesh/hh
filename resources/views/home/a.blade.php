@extends('layouts.home')

@section('content')
<!--banner-section-->
<section class="partner-sec block-element">
	<div class="container">
		<img src="assets/images/hand.png" alt="">
		<h2>Become Partner With <br>US</h2>
		<p>We invite travel agents, hoteliers or other commercial lodging establishments, integrated service providers and even advertisers to partner with
			us. Ourpartners can benefit immensely via Trabug in providing unique and experiential services and information to our customers, along with us.</p>
		<div class="partner-form partner-main-form">
			<form>
				<div class="row">
					<div class="col-sm-6">
						<div class="inner-fom">
							<div class="form-group">
								<label>Name</label>
								<span><input type="text" placeholder="abcd@gmail.com" value=""></span>
							</div>
							<div class="form-group">
								<label>E-mail ID</label>
								<span><input type="text" placeholder="abcd@gmail.com" value=""></span>
							</div>
							<div class="form-group">
								<label>Contact No.</label>
								<span><input type="text" placeholder="abcd@gmail.com" value=""></span>
							</div>
							<div class="form-group">
								<label>Company/Agency
									Website </label>
								<span><input type="text" placeholder="abcd@gmail.com" value=""></span>
							</div>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="inner-fom">
							<div class="form-group textarea">
								<textarea id="w3review" name="w3review" rows="4" cols="50" placeholder="Brief Description"></textarea>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</section>


@endsection