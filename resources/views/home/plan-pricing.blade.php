@extends('layouts.home')

@section('content')
<!--banner-section-->
<section class="paln-new-page">
    <h2>MOBILE PLANS & PRICING</h2>
    <div class="plan-customer">
        <div class="row">
            <div class="col-sm-5">
                <div class="plan-customer-map">
                    <img src="{{ asset('/images/map.png') }}" alt="">
                </div>
            </div>
            <div class="col-sm-7">
                <div class="plan-customer-ser">
                    <div class="row">
                        @foreach($mobilePlans as $key => $mobilePlan)
                        <div class="col-sm-4">
                            <div class="plan-blog plan-{{$container[$key]}}">
                                <div class="basic-block">
                                    <div class="basic-price">
                                        <label> {{$mobilePlan->price}}/- <span>Per day</span> </label>
                                        <img src="{{ asset('/images/nw-'.($key+1).'-gr.jpg') }}" alt="">
                                    </div>
                                    <h4>{{$mobilePlan->name}}</h4>
                                </div>
                                <span>Calling : {{$mobilePlan->calling}}</span>
                                <span>Data : {{$mobilePlan->data}}</span>
                                <a class="bl-btn" href="book-now?deviceid=1&planid={{$mobilePlan->id}}">Book NOW</a>
                            </div>
                        </div>
                        @endforeach

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="paln-new-page">
    <h2>Wi-Fi PLANS & PRICING</h2>
    <div class="plan-customer wi-fi-new-plans">
        <div class="row">
            <div class="col-sm-5">
                <div class="plan-customer-map">
                    <img src="{{ asset('/images/map.png') }}" alt="">
                </div>
            </div>
            <div class="col-sm-7">
                <div class="plan-customer-ser">
                    <div class="row">
                        @foreach($wifiPlans as $key => $wifiPlan)
                        <div class="col-sm-4 wow fadeInLeft animate">
                            <div class="wifi-inner-blog {{$container[$key]}}">
                                <h4>{{$wifiPlan->name}}</h4>
                                <div class="wifi-img">
                                    <div class="wifi-price">
                                        <label>{{$wifiPlan->price}}/-
                                            <span>Per day</span></label>
                                        <img src="{{ asset('/images/wi-cr-'.($key+1).'.jpg') }}" alt="">
                                    </div>
                                </div>
                                <span>Data : {{$wifiPlan->data}}</span>
                                <a class="bl-btn" href="book-now?deviceid=2&planid={{$wifiPlan->id}}">Book NOW</a>
                            </div>
                        </div>

                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection