@extends('layouts.home')

@section('content')
<!--banner-section-->
@php
$planPrice = 0;
$qnt = 1;
@endphp
<section class="login-pahe-main book-now block-element">
    <div class="container">
        <div class="row">
            <div class="col-sm-9">
                <div class="login-lft">
                    <div class="login-rgt">
                        <div class="partner-form">
                            <form autocomplete="off" id="bookNowForm">
                                <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                                <div class="inner-fom" id="bookDevice" data-sectionValue="0">
                                    <h5>For existing subscriber only. If you are new user, kindly book your plan first
                                    </h5>
                                    <div class="form-group" id="emailClone">
                                        <label>Email </label>
                                        <span><input type="text" name="email" placeholder="Please enter your email id"
                                                value="" require></span>
                                    </div>
                                    <div class="form-group" id="rentalClone">
                                        <label>Rental Dates</label>
                                        <span>
                                            <div class="clander">
                                                <label>Start</label>
                                                <input type="text" placeholder="DD/MM/YYYY" value="" id="start_date"
                                                    name="start_date" readonly class="deviceDate">
                                            </div>
                                            <div class="clander">
                                                <label>End</label>
                                                <input type="text" placeholder="DD/MM/YYYY" value="" id="end_date"
                                                    name="end_date" readonly class="deviceDate">
                                            </div>
                                        </span>
                                    </div>
                                    <div class="form-group">
                                        <label>Select Device</label>
                                        <span>
                                            <div class="clander">
                                                <select name="cars" id="device">
                                                    <option value="">Select device </option>
                                                    @foreach(config('constant.DEVICE_TYPE') as $key => $device_type)
                                                    <option value="{{ $key }}"
                                                        {{ request()->deviceid == $key ? 'selected' : '' }}>
                                                        {{ $device_type }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="clander">
                                                <label>Quantity</label>
                                                <select name="quantity[]" id="quantity">
                                                    @for ($i = 1; $i <= 10; $i++) <option value="{{ $i }}">{{ $i }}
                                                        </option>
                                                        @endfor
                                                </select>
                                            </div>
                                        </span>
                                    </div>
                                    <div class="form-group full-row">
                                        <label>Choose Plan</label>
                                        <span>
                                            <div class="clander">
                                                <select name="cars1" id="plans">
                                                    <option>Select plan</option>
                                                    @foreach($plans as $plan)
                                                    <option data-id="{{$plan->id}}" data-price="{{$plan->price}}"
                                                        vlaue="{{$plan->id}}" @php if(request()->planid == $plan->id){
                                                        echo 'selected';
                                                        $planPrice = $plan->price;
                                                        }
                                                        @endphp
                                                        >
                                                        {{$plan->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </span>
                                        <input type="hidden" id="hiddenPrice" name="hiddenPrice[]"
                                            value="{{$planPrice}}">
                                    </div>
                                    <div class="form-group accesory">
                                        <label>Any Accessory?</label>
                                        <span>
                                            <div class="clander">
                                                <select name="cars" id="accessories">
                                                    <option value>Select Accessories</option>
                                                    @foreach($accessories as $accessorie)
                                                    <option value="{{$accessorie->id}}"
                                                        data-price="{{$accessorie->price}}">
                                                        {{$accessorie->accessorie_name}} ({{$accessorie->description}})
                                                    </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="clander">
                                                <label>Quantity</label>
                                                <select name="cars" id="accessorieQnt">
                                                    @for ($i = 1; $i <= 10; $i++) <option value="{{ $i }}">{{ $i }}
                                                        </option>
                                                        @endfor
                                                </select>
                                            </div>
                                        </span>
                                    </div>
                                    <span class="btn btn-danger delete hidden" id="removeBtn" title="Remove device"
                                        data-section="bookDevice" style="float:right;"> × </span>
                                </div>
                                <div class="form-group">
                                    <label></label>
                                    <a class="bl-btn" id="AddMoreBtn" onclick="addMore('bookDevice')"
                                        href="javascript:void(0)">Add Another Device</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="payment-bills pay">
                    <input type="hidden" id="totalAmit" value="">
                    <div class="total-payment">
                        <div class="total-price" style="position: absolute;margin-top: 35px;">
                            <span id="deviceTable"></span>
                            <span id="accessorieTable"></span>
                            <span id="priceTable"></span>
                        </div>
                    </div>
                    <div class="pay-ment">
                        <button class="btn btn-success" id="proceedPayment" onclick="proceedPayment(this)">Proceed to
                            payment</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- sppiner -->

<div class='load' style="display: none;">
    <div class='alert_msg'>
        <span>Processing... please wait!</span>
        <p>[Please do not refresh and do not press back button.]</p>
    </div>
    <div class='preloader-js-container'>
        <div class='shadow'></div>
        <div class='preloader-js'>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div>
</div>
<!--  -->
@endsection
@section('javascript')
<!-- spinner -->
<link rel="stylesheet" href="{{ asset('/css/spinner.css') }}">
<!-- Date-picker-css -->
<link rel="stylesheet" href="{{ asset('/css/bootstrap-datepicker.min.css') }}">
<!-- Date-picker-js -->
<script src="{{ asset('js/moment.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('js/jquery.validate.min.js') }}"></script>
<script>
var sectionValue = 0;
var days = [];
var qnt = [];
var price = [];
var items = [];
var deviceId = [];
var planId = [];
deviceId[sectionValue] = "<?php echo $_GET['deviceid'] ?>";
planId[sectionValue] = "<?php echo $_GET['planid'] ?>";
price[sectionValue] = $("#hiddenPrice").val();
qnt[sectionValue] = 1;
renderDeviceTable();
renderAccessorieTable();
calculatePrice(0);

//getting start date and end date
$('#start_date').datepicker({
    format: "dd/mm/yyyy",
    autoclose: true,
    startDate: '-0m',
}).on('changeDate', function(selected) {
    var sectionId = $(this).closest('div.inner-fom').attr('data-sectionValue');
    var startdate = $("#start_date").val();
    var momentStartDate = moment(startdate, 'DD/MM/YYYY');
    if ($("#end_date").val() != "") {
        var momentEnddate = moment($("#end_date").val(), 'DD/MM/YYYY');
        days[sectionId] = calDays(momentStartDate, momentEnddate);
        pushNewDevice(sectionId);
    }
    $('#end_date').datepicker({
        format: "dd/mm/yyyy",
        autoclose: true,
        startDate: startdate,
    }).on("changeDate", function(selected) {
        var momentEnddate = moment($("#end_date").val(), 'DD/MM/YYYY');
        days[sectionId] = calDays(momentStartDate, momentEnddate);
        pushNewDevice(sectionId);
    });
});

//Add another device
function addMore(sectionId = null) {
    if (sectionId != null) {
        sectionValue++
        var length = $("#" + sectionId).parent().find("div.inner-fom").length;
        if (length < 6) {
            var cloneHtml = $("#" + sectionId).clone().attr('data-sectionValue', sectionValue)
            cloneHtml.find("input#start_date").val('');
            cloneHtml.find("input#end_date").val('');
            cloneHtml.find("h5").remove();
            cloneHtml.find("#emailClone").remove();
            cloneHtml.find("label.error").remove();
            cloneHtml.find(".hidden").removeClass('hidden');
            cloneHtml.insertBefore($('#AddMoreBtn').parent('div'));
            cloneHtml.find('input#start_date')
                .removeClass('hasDatepicker')
                .removeData('datepicker')
                .unbind()
                .datepicker({
                    format: "dd/mm/yyyy",
                    autoclose: true,
                    startDate: '-0m',
                }).on('changeDate', function(selected) {
                    var currentDiv = $(this).closest('div.inner-fom');
                    var cloneStartDate = currentDiv.find("#start_date").val();
                    var cMomentStartDate = moment(cloneStartDate, 'DD/MM/YYYY');
                    var sectionDataId = currentDiv.attr("data-sectionValue");
                    if (currentDiv.find("#end_date").val() != "") {
                        var cMomentEnddate = moment(currentDiv.find("#end_date").val(), 'DD/MM/YYYY');
                        days[sectionDataId] = calDays(cMomentStartDate, cMomentEnddate);
                        pushNewDevice(sectionDataId);
                    }
                    cloneHtml.find('input#end_date')
                        .removeClass('hasDatepicker')
                        .removeData('datepicker')
                        .unbind()
                        .datepicker({
                            format: "dd/mm/yyyy",
                            autoclose: true,
                            startDate: cloneStartDate
                        }).on('changeDate', function(selected) {
                            var cMomentEnddate = moment(currentDiv.find("#end_date").val(), 'DD/MM/YYYY');
                            days[sectionDataId] = calDays(cMomentStartDate, cMomentEnddate);
                            pushNewDevice(sectionDataId);
                        });
                });
            qnt[sectionValue] = 1;
            price[sectionValue] = $("#hiddenPrice").val();
            deviceId[sectionValue] = "<?php echo $_GET['deviceid'] ?>";
            planId[sectionValue] = "<?php echo $_GET['planid'] ?>";
        }
        if (length == 5) {
            $('#AddMoreBtn').hide('slow');
        }
    } else {
        return false;
    }
}

//Calculate no. of dasys
function calDays(start, end) {
    return moment.duration(end.diff(start)).days() + 1;
}

//On device quantity change
$(document).on('change', "#quantity", function() {
    var sectionId = $(this).closest('div.inner-fom').attr("data-sectionValue");
    qnt[sectionId] = $(this).val();
    pushNewDevice(sectionId);
});

//get plan using ajax
$(document).on("change", "#device", function(e) {
    var $el = $(this);
    var selectedDeviceId = $el.val();

    if (selectedDeviceId != '') {
        $.get('/getDevicePlan/' + selectedDeviceId, function(response) {
            if (response.length > 0) {
                var sectionId = $el.closest('div.inner-fom').attr("data-sectionValue");
                price[sectionId] = response[0].price;
                deviceId[sectionId] = selectedDeviceId;
                setTimeout(function() {
                    planId[sectionId] = response[0].id;
                }, 1000);
                pushNewDevice(sectionId);
                var plans = $el.closest('div.inner-fom').find('#plans').empty();
                $.each(response, function(key, val) {
                    $("<option data-price=" + val.price + " value=" + val.id + ">" + val.name +
                        "</option>").appendTo(plans);
                });
            }
        });
    } else {
        return false;
    }
});

//get plan price when plan change 
$(document).on('change', "#plans", function(e) {
    var planPrice = $(this).find(':selected').data("price");
    var sectionId = $(this).closest('div.inner-fom').attr("data-sectionValue");
    price[sectionId] = planPrice;
    planId[sectionId] = $(this).find(":selected").data("id");
    console.log(planId);
    pushNewDevice(sectionId);
});

//get accessories price on change
var accessoriePrc = [];
var accessorieQnt = [];
var accessorieId = [];
$(document).on('change', "#accessories", function(e) {
    var parentDiv = $(this).closest('div.inner-fom');
    var sectionId = parentDiv.attr("data-sectionValue");
    var accessoriesPrice = $(this).find(':selected').data("price");
    accessorieId[sectionId] = $(this).find(':selected').val();
    accessoriePrc[sectionId] = accessoriesPrice;
    accessorieQnt[sectionId] = parentDiv.find("#accessorieQnt").val();
    pushNewDevice(sectionId);
});

//On Accessorie quantity change
$(document).on('change', "#accessorieQnt", function() {
    var sectionId = $(this).closest('div.inner-fom').attr("data-sectionValue");
    accessorieQnt[sectionId] = $(this).val();
    pushNewDevice(sectionId);
});

// Remove device
$(document).on('click', "#removeBtn", function(event) {
    var removeSection = $(this).closest('div.inner-fom');
    var removeSectionId = removeSection.attr("data-sectionValue");
    Object.entries(items).forEach(([k, v]) => {
        if (k === removeSectionId) delete items[k];
    });
    removeSection.remove();
    renderDeviceTable();
    renderAccessorieTable();
    $('#AddMoreBtn').show('slow');
});

//Calculate no. of device
function pushNewDevice(sectionId) {
    if (days != 0 && price != 0 && qnt != 0) {
        var newItem = {
            deviceId: deviceId[sectionId],
            planId: planId[sectionId],
            days: days[sectionId],
            price: price[sectionId],
            qnt: qnt[sectionId],
            accessorie: {
                accessorieId: accessorieId[sectionId],
                accessoriePrc: accessoriePrc[sectionId],
                accessorieQnt: accessorieQnt[sectionId],
            }
        }
        items[sectionId] = newItem;
        if (items.length > 0) {
            renderDeviceTable();
            renderAccessorieTable();
        }
    }
}

//Create device table
function renderDeviceTable() {
    var html = '';
    var ele = document.getElementById("deviceTable");
    var deviceTotal = 0;
    ele.innerHTML = '';
    html += "<table class='table table-sm table-hover'>";
    html += "<tr><tr><th colspan='4'>Devices</th></tr>";
    html += "<tr><th>Days</th>";
    html += "<th>Quantity</th>";
    html += " <th>Pice</th>";
    html += "<th>Total</th></tr>";

    if (items.length > 0) {
        Object.keys(items).forEach(([key, val]) => {
            if (items[key].days) {
                html += "<tr>";
                html += "<td>" + items[key].days + "</td>";
                html += "<td>" + items[key].qnt + "</td>";
                html += "<td>" + items[key].price + "</td>";
                html += "<td>" + parseFloat(items[key].days * items[key].qnt * items[key].price) + "</td>";
                html += "</tr>";
                deviceTotal += parseFloat(items[key].price) * parseInt(items[key].qnt) * parseInt(items[key]
                    .days);
            }
        });
        html += "<tr>";
        html += "<td colspan='3'>Total:</td>";
        html += "<td colspan='1'>" + deviceTotal + "</td>";
        html += "</tr>";
    } else {
        html += "<tr><td colspan='4'>No device found..!</td></tr>";
    }
    html += "</table>";
    ele.innerHTML = html;
}

//Create  Accessorie Table;
function renderAccessorieTable() {
    var html = '';
    var ele = document.getElementById("accessorieTable");
    var accessorieTotal = 0;
    var deviceTotal = 0;
    var priceShow = false;
    ele.innerHTML = '';
    html += "<table class='table table-sm table-hover'>";
    html += "<tr><tr><th colspan='4'>Accessorie</th></tr>";
    html += "<tr><th>Days</th>";
    html += "<th>Quantity</th>";
    html += " <th>Pice</th>";
    html += "<th>Total</th></tr>";

    if (items.length > 0) {
        Object.keys(items).forEach(([key, val]) => {
            deviceTotal += parseFloat(items[key].price) * parseInt(items[key].qnt) * parseInt(items[key].days);
            if (items[key].days && typeof(items[key].accessorie.accessoriePrc) != "undefined") {
                priceShow = typeof(items[key].accessorie.accessoriePrc) != "undefined" ? true : false;
                html += "<tr>";
                html += "<td>" + items[key].days + "</td>";
                html += "<td>" + items[key].accessorie.accessorieQnt + "</td>";
                html += "<td>" + items[key].accessorie.accessoriePrc + "</td>";
                html += "<td>" + parseFloat(items[key].days * items[key].accessorie.accessorieQnt * items[key]
                    .accessorie.accessoriePrc) + "</td>";
                html += "</tr>";
                accessorieTotal += parseFloat(items[key].accessorie.accessoriePrc) * parseInt(items[key]
                    .accessorie.accessorieQnt) * parseInt(items[key].days);
            }
        });
        if (priceShow) {
            html += "<tr>";
            html += "<td colspan='3'>Total:</td>";
            html += "<td colspan='1'>" + accessorieTotal + "</td>";
            html += "</tr>";
        } else {
            html += "<tr><td colspan='4'>No accessorie found..!</td></tr>";
        }
        //var totalPrice = !isNaN(deviceTotal) ? deviceTotal : "0"; + !isNaN(accessorieTotal) ? accessorieTotal : "0";
        var totalPrice = deviceTotal + accessorieTotal;
        calculatePrice(totalPrice);
    } else {
        html += "<tr><td colspan='4'>No accessorie found..!</td></tr>";
    }
    html += "</table>";
    ele.innerHTML = html;
}

//final price calculation
function calculatePrice(totalPrice) {
    $("#totalAmit").val(totalPrice);
    var html = '';
    var ele = document.getElementById("priceTable");
    ele.innerHTML = '';
    html += "<table class='table table-sm table-hover'>";
    html += "<tr><th colspan='3'>Subtotal:</th>";
    html += "<td>" + totalPrice + "</td></tr>";
    html += "<tr><th colspan='3'>Total:</th>";
    html += "<td>" + totalPrice + "</td></tr>";
    html += "</table>";
    ele.innerHTML = html;
}

function proceedPayment(e) {
    var valid = $("#bookNowForm").valid();
    if (!valid) {
        return false;
    } else {
        if (items.length > 0) {
            var email = $("input[name='email']").val();
            var deviceFormData = {
                'items': items,
                'email': email,
                'toatlAmt': $("#totalAmit").val()
            };
            saveFormData(deviceFormData);
        }
    }
}

function saveFormData(deviceFormData) {
    
    $.ajax({
        url: 'bookDevice',
        data: {
            deviceFormData,
            "_token": $('#token').val()
        },
        type: 'POST',
        beforeSend: function() {
            $("#proceedPayment").attr('disabled', 'disabled');
            $('.book-now.block-element').addClass('disable');
            $('.load').show();
            $('.book-now.block-element.disable').fadeTo('slow', .6);
            $('.book-now.block-element.disable').append(
                '<div class="fadeContainer" style="position: absolute;top:0;left:0;width: 100%;height:100%;z-index:2;opacity:0.4;filter: alpha(opacity = 50)"></div>'
            );
        },
        success: function(response) {
            var res = JSON.parse(response);
            $("#proceedPayment").removeAttr('disabled');
            $('.book-now.block-element').removeClass('disable');
            $('.book-now.block-element').removeAttr('style');
            $('.load').hide();
            $('.book-now.block-element').find('.fadeContainer').remove();
            if (res.status == 200) {
                window.location.href = "http://127.0.0.1:8000/procees-page/" + res.orderId;
                //window.location.href =  window.location.protocol+"//"+window.location.hostname+"/procees-page/"+res.orderId ;
            } else {
                console.log(res.result)
            }
        }
    });
}
</script>
@endsection