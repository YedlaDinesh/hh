@extends('layouts.home')

@section('content')
<!--banner-section-->

<section class="login-pahe-main block-element">
    <div class="container">
        <div class="row">
            <div class="col-sm-5">
                <div class="login-lft">
                    <img src="{{ asset('/images/login.png') }}" alt="">
                    <h2>Welcome To<br> Com. name</h2>
                </div>
            </div>
            <div class="col-sm-7">
                <h5>For existing subscriber only. If you are new user, kindly book your plan first </h5>
                <div class="login-rgt">
                    <div class="partner-form">
                        <form role="form" method="post" action="" id="waitform" enctype="multipart/form-data">
                            <input type="hidden" value="{{csrf_token()}}" name="_token" />

                            <div class="inner-fom">
                                <div class="form-group">
                                    <label>Order No</label>
                                    <span><input type="text" name="orderid" placeholder="Order no" value=""></span>
                                </div>
                                <div class="form-group">
                                    <label>Email</label>
                                    <span><input type="text" name="email" placeholder="Email" value=""></span>
                                </div>
                                <input type="submit" value="Log In" placeholder="">
                            </div>
                        </form>
                    </div>
                    <p>We request our subscriber to complete the booking process at least 7 days prior
                        to rental start date. In case of cancellation due to the incomplete booking process
                        charges may apply as per terms and conditions mentioned on our website</p>
                    <p>If you required any support in continuation of booking process then you can raise
                        a ticket and our support team will contact you.</p>

                </div>
            </div>
        </div>
    </div>
</section>

@endsection