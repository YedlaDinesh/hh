@extends('layouts.home')

@section('content')
	<!--Faq-section-->

    <section class="faq-page block-element">
<div class="container">
<h2>Frequently asked questions</h2>
	<div class="row">
        <!-- General Q&A -->
		  <div class="col-sm-6">
			  <div class="faq-inner">
				<span>General questions</span>
				   <div id="accordiongeneral">
                   @foreach ($general as $key => $generalQA) 
                 <div class="card">
                    <div class="card-header">
                        <a class="card-link" data-toggle="collapse" href="#collapseOne{{$key}}">
                       {{$generalQA->question}}
                        </a>
                    </div>
                    <div id="collapseOne{{$key}}" class="{{  $key ==  '0' ? 'collapse show' : 'collapse'  }}" data-parent="#accordiongeneral">
                        <div class="card-body">
                        {{$generalQA->answer}}
                        </div>
                    </div>
                    </div>
                    @endforeach
                 </div>
			      </div>
		  </div>
      <!-- Technical Q&A -->
			<div class="col-sm-6">
			  <div class="faq-inner">
				<span>Technical questions</span>
				   <div id="accordiontechnical">
                   @foreach ($technical as $key => $technicalQA) 
                 <div class="card">
                    <div class="card-header">
                        <a class="card-link" data-toggle="collapse" href="#collapseOne{{$key}}">
                       {{$technicalQA->question}}
                        </a>
                    </div>
                    <div id="collapseOne{{$key}}" class="{{  $key ==  '20' ? 'collapse show' : 'collapse'  }}" data-parent="#accordiontechnical">
                        <div class="card-body">
                        {{$technicalQA->answer}}
                        </div>
                    </div>
                    </div>
                    @endforeach
                 </div>
			      </div>
		  </div>

	</div>

  <!-- Second row -->

  <div class="row">
        <!-- Pocket wifi Q&A -->
		  <div class="col-sm-6">
			  <div class="faq-inner">
				<span>My pocket wifi</span>
				   <div id="accordionpocket">
                   @foreach ($pocket as $key => $pocketQA) 
                 <div class="card">
                    <div class="card-header">
                        <a class="card-link" data-toggle="collapse" href="#collapseOne{{$key}}">
                       {{$pocketQA->question}}
                        </a>
                    </div>
                    <div id="collapseOne{{$key}}" class="collapse " data-parent="#accordionpocket">
                        <div class="card-body">
                        {{$pocketQA->answer}}
                        </div>
                    </div>
                    </div>
                    @endforeach
                 </div>
			      </div>
		  </div>
      <!-- Delivery Q&A -->
			<div class="col-sm-6">
			  <div class="faq-inner">
				<span>Delivery and return</span>
				   <div id="accordiondelivery">
                   @foreach ($delivery as $key => $deliveryQA) 
                 <div class="card">
                    <div class="card-header">
                        <a class="card-link" data-toggle="collapse" href="#collapseOne{{$key}}">
                       {{$deliveryQA->question}}
                        </a>
                    </div>
                    <div id="collapseOne{{$key}}" class="collapse " data-parent="#accordiondelivery">
                        <div class="card-body">
                        {{$deliveryQA->answer}}
                        </div>
                    </div>
                    </div>
                    @endforeach
                 </div>
			      </div>
		  </div>

	</div>

  <!-- Third Row -->


  <div class="row">
        <!-- Network Q&A -->
		  <div class="col-sm-6">
			  <div class="faq-inner">
				<span>Network speed and data usage</span>
				   <div id="accordionnetwork">
                   @foreach ($network as $key => $networkQA) 
                 <div class="card">
                    <div class="card-header">
                        <a class="card-link" data-toggle="collapse" href="#collapseOne{{$key}}">
                       {{$networkQA->question}}
                        </a>
                    </div>
                    <div id="collapseOne{{$key}}" class="collapse " data-parent="#accordionnetwork">
                        <div class="card-body">
                        {{$networkQA->answer}}
                        </div>
                    </div>
                    </div>
                    @endforeach
                 </div>
			      </div>
		  </div>
      <!-- Payment Q&A -->
			<div class="col-sm-6">
			  <div class="faq-inner">
				<span>Payment questions</span>
				   <div id="accordionpayment" >
                   @foreach ($payment as $key => $paymentQA) 
                 <div class="card">
                    <div class="card-header">
                        <a class="card-link" data-toggle="collapse" href="#collapseOne{{$key}}">
                       {{$paymentQA->question}}
                        </a>
                    </div>
                    <div id="collapseOne{{$key}}" class="collapse " data-parent="#accordionpayment">
                        <div class="card-body">
                        {{$paymentQA->answer}}
                        </div>
                    </div>
                    </div>
                    @endforeach
                 </div>
			      </div>
		  </div>

	</div>

</div>
</section>
@endsection