@extends('layouts.home')

@section('content')
<!--banner-section-->
<section class="login-pahe-main ticket-page block-element">
    <div class="container">
        @if($rets['status'] != "")
        <div class="alert alert-{{$rets['status']}} alert-dismissible fade show" role="alert">
            <strong></strong>{{$rets['result']}}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        @endif
        <h2>Raise Your Voice <br> Instantly</h2>
        <h5>(For Existing Customer only)</h5>

        <div class="login-rgt">
            <div class="partner-form">
                <form method="post" role="form" action="" autocomplete="false">
                    <input type="hidden" value="{{csrf_token()}}" name="_token" />
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="inner-fom">
                                <div class="form-group">
                                    <label>Name</label>
                                    <span><input type="text" placeholder="Name" value="{{old('name')}}" name="name">
                                        @error('name')
                                        <div class="error" role="alert">
                                            {{ $errors->first('name') }}
                                        </div>
                                        @enderror
                                    </span>
                                </div>
                                <div class="form-group">
                                    <label>E-mail ID</label>
                                    <span><input type="email" placeholder="Email" value="{{old('email')}}" name="email"
                                            required>
                                        @error('email')
                                        <div class="error" role="alert">
                                            {{ $errors->first('email') }}
                                        </div>
                                        @enderror
                                    </span>
                                </div>
                                <div class="form-group">
                                    <label>Order No.<span>(For Existing Customer)</span></label>
                                    <span><input type="text" placeholder="Order no" value="{{old('orderId')}}"
                                            name="orderId" required>
                                        @error('orderId')
                                        <div class="error" role="alert">
                                            {{ $errors->first('orderId') }}
                                        </div>
                                        @enderror
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="inner-fom">
                                <div class="form-group textarea">
                                    <textarea id="w3review" rows="4" cols="50"
                                        placeholder="Type your issue here. If you are our existing customer please mentioned your order no to resolve the issue on priority"
                                        name="query" required>{{old('query')}}</textarea>
                                    @error('query')
                                    <div class="error" role="alert">
                                        {{ $errors->first('query') }}
                                    </div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">

                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <button><img src="{{ asset('/images/ticket.png') }}" alt=""></button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection