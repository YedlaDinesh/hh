@extends('layouts.home')

@section('content')
<!--banner-section-->

<section class="blog-page-main block-element">
    <div class="container">
        <div class="row">
            <div class="col-sm-9">
                <div class="blog-content">
                    <div class="blog-content-inner">
                        <img src="{{ URL::to('/').'/images/blogs/'.$blog_post->image }}" alt="">
                        <span>{{$blog_post->title}}</span>
                        <div class="content">
                            {!!$blog_post->description !!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="blog-content-list">
                    <div class="recen-post">
                        <span>Recent Posts</span>
                        <ul>
                            @foreach ($recent_post as $key => $recent)
                            <li>
                                <a href="{{url('blog/'.$recent->slug)}}">{{ $recent->title }}e</a>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="blog-clouds">
                        <span>Blog Cloud</span>
                        @foreach ($categories as $key => $categorie)
                        <a href="{{ url('blogs?category='.$categorie->id) }}">{{$categorie->category_name}}</a>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection