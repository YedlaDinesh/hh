@extends('layouts.home')

@section('content')
<section class="review-page block-element">
<div class="container">
<h2>Frequently asked questions</h2>
	<div class="row">
		<div class="col-sm-4">
			<div class="review-main">
				<div class="user-in">
					<img src="{{ asset('/images/OIP.png') }}" alt="">
					<div class="user-detail">
						<h6>John Deo</h6>
						<div class="rating">
							<span class="fa fa-star checked"></span>
							<span class="fa fa-star checked"></span>
							<span class="fa fa-star checked"></span>
							<span class="fa fa-star"></span>
							<span class="fa fa-star"></span>
						</div>
						<p>Mar 16, 2020</p>
					</div>
				</div>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
			</div>
		</div>
		<div class="col-sm-4">
			<div class="review-main">
				<div class="user-in">
					<img src="{{ asset('/images/OIP.png') }}" alt="">
					<div class="user-detail">
						<h6>John Deo</h6>
						<div class="rating">
							<span class="fa fa-star checked"></span>
							<span class="fa fa-star checked"></span>
							<span class="fa fa-star checked"></span>
							<span class="fa fa-star"></span>
							<span class="fa fa-star"></span>
						</div>
						<p>Mar 16, 2020</p>
					</div>
				</div>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
			</div>
		</div>
		<div class="col-sm-4">
			<div class="review-main">
				<div class="user-in">
					<img src="{{ asset('/images/OIP.png') }}" alt="">
					<div class="user-detail">
						<h6>John Deo</h6>
						<div class="rating">
							<span class="fa fa-star checked"></span>
							<span class="fa fa-star checked"></span>
							<span class="fa fa-star checked"></span>
							<span class="fa fa-star"></span>
							<span class="fa fa-star"></span>
						</div>
						<p>Mar 16, 2020</p>
					</div>
				</div>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-4">
			<div class="review-main">
				<div class="user-in">
					<img src="{{ asset('/images/OIP.png') }}" alt="">
					<div class="user-detail">
						<h6>John Deo</h6>
						<div class="rating">
							<span class="fa fa-star checked"></span>
							<span class="fa fa-star checked"></span>
							<span class="fa fa-star checked"></span>
							<span class="fa fa-star"></span>
							<span class="fa fa-star"></span>
						</div>
						<p>Mar 16, 2020</p>
					</div>
				</div>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
			</div>
		</div>
		<div class="col-sm-4">
			<div class="review-main">
				<div class="user-in">
					<img src="{{ asset('/images/OIP.png') }}" alt="">
					<div class="user-detail">
						<h6>John Deo</h6>
						<div class="rating">
							<span class="fa fa-star checked"></span>
							<span class="fa fa-star checked"></span>
							<span class="fa fa-star checked"></span>
							<span class="fa fa-star"></span>
							<span class="fa fa-star"></span>
						</div>
						<p>Mar 16, 2020</p>
					</div>
				</div>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
			</div>
		</div>
		<div class="col-sm-4">
			<div class="review-main">
				<div class="user-in">
					<img src="{{ asset('/images/OIP.png') }}" alt="">
					<div class="user-detail">
						<h6>John Deo</h6>
						<div class="rating">
							<span class="fa fa-star checked"></span>
							<span class="fa fa-star checked"></span>
							<span class="fa fa-star checked"></span>
							<span class="fa fa-star"></span>
							<span class="fa fa-star"></span>
						</div>
						<p>Mar 16, 2020</p>
					</div>
				</div>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
			</div>
		</div>
	</div>
</div>
</section>
@endsection