@extends('layouts.home')

@section('content')
	<!--banner-section-->

<section class="login-pahe-main payment-page block-element">
<div class="container">
	<div class="row">
		<div class="col-sm-5">
			<div class="login-lft">
				<img src="{{ asset('/images/login.png') }}" alt="">
				<h2>Welcome To<br> Com. name</h2>
			</div>
		</div>
		<div class="col-sm-7">
			<div class="login-rgt">
				<div class="partner-form">
					<form>
					<div class="inner-fom">
					<h6>Your Order Number is {{$id}}</h6>
					<h6>Order Number is also sent to you by email</h6>
					<h6>Kindly mention it in every communication with Us</h6>
					<input type="submit" value="Continue Booking Process" placeholder="">
					</div>
					</form>
					</div>
				
			</div>
		</div>
	</div>
</div>
</section>

@endsection