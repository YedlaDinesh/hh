@extends('layouts.home')

@section('content')
	<!--banner-section-->
<section class="ref-bnr block-element">
	<div class="ref-overlay">
		<p>Earn Unlimited by Getting your Near Ones Connected</p>
	</div>
</section>
<section class="refer-step block-element">
<div class="container">
	<div class="row">
		<div class="col-sm-5">
			<img src="{{ asset('/images/stroller.png') }}" alt="">
		</div>
		<div class="col-sm-7">
			<div class="refer-step-inner">
				<ul>
					<li><span>Step 1</span>Click on “Refer Now” to join the referral Program of HH</li>
					<li><span>Step 2</span>Get your Unique referral code</li>
					<li><span>Step 3</span>Share the referral code with your near ones or clients for booking</li>
					<li><span>Step 4</span>Earn as per Referrals by you. More Booking & More Earning</li>
				</ul>
			</div>
			<a class="bl-btn" href="#">Refer Now</a>
		</div>
	</div>
</div>
</section>

@endsection