@extends('layouts.user')
@section('content')
<div class="row">
    <div class="col-md-9 col-center">
        <section class="content-header">
            <h1>Add Accessorie </h1>
            @if(\Session::has('danger'))
            <br />
            <div class="alert alert-danger">
                {{\Session::get('danger')}}
            </div>
            @endif
        </section>
        <section class="content">
            <form role="form" method="post" action="" id="waitform" autocomplete="off">
                <!-- general form elements disabled -->
                <div class="box">
                    <div class="box-header">
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-12">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>Accessorie Name</label>
                                            <input type="hidden" value="{{csrf_token()}}" name="_token" />
                                            <input type="text" name="accessorie_name" class="form-control" placeholder="Please enter accessorie name" value="{{old('accessorie_name') ?? $accessorie->accessorie_name}}" required />
                                            @error('accessorie_name')
                                            <span class="invalid-feedback" role="alert">
                                                {{ $errors->first('accessorie_name') }}
                                            </span>
                                            @enderror
                                        </div>
                                        
                                         <div class="form-group">
                                            <label>Price</label>
                                            <input type="hidden" value="{{csrf_token()}}" name="_token" />
                                            <input type="number" name="price" class="form-control" placeholder="Please enter accessorie price" value="{{old('price') ?? $accessorie->price}}" required />
                                            @error('price')
                                            <span class="invalid-feedback" role="alert">
                                                {{ $errors->first('price') }}
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Description</label>
                                            <input type="hidden" value="{{csrf_token()}}" name="_token" />
                                            <input type="text" name="description" class="form-control" placeholder="Please enter description" value="{{old('description') ?? $accessorie->description}}" required />
                                            @error('description')
                                            <span class="invalid-feedback" role="alert">
                                                {{ $errors->first('description') }}
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <a href="{{ url('admin/accessorie/list') }}" class="btn btn-danger">&larr; Cancel</a> <button type="submit" class="btn btn-primary pull-right">Submit</button>
                    </div>
                </div>
            </form>
        </section>
    </div>
</div>
@endsection