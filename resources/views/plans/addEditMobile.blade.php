@extends('layouts.user')

@section('content')

<div class="row">
    <div class="col-md-9 col-center">
        <section class="content-header">
            <h1>Edit Mobile Plan </h1>
            @if(\Session::has('danger'))
            <br />
            <div class="alert alert-danger">
                {{\Session::get('danger')}}
            </div>
            @endif
        </section>
        <section class="content">
            <form role="form" method="post" action="" id="waitform">
                <!-- general form elements disabled -->
                <div class="box">
                    <div class="box-header">
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                
                            <div class="row">
                                        <!-- text input -->
                                        <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Name</label>
                                            <input type="hidden" value="{{csrf_token()}}" name="_token" />
                                            <input type="text" name="name" class="form-control" placeholder="Please enter Name" value="{{old('name') ?? $plan->name}}" required />
                                            @error('name')
                                            <span class="invalid-feedback" role="alert">
                                                {{ $errors->first('name') }}
                                            </span>
                                            @enderror
                                        </div>
                                        </div>
                                        <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Price</label>
                                            <input type="hidden" value="{{csrf_token()}}" name="_token" />
                                            <input type="number" name="price" class="form-control" placeholder="Please enter Price" value="{{old('price') ?? $plan->price}}" required />
                                            @error('price')
                                            <span class="invalid-feedback" role="alert">
                                                {{ $errors->first('price') }}
                                            </span>
                                            @enderror
                                        </div>
                                        </div>                     
                                        </div>

                                        <div class="row">
                                        <!-- text input -->
                                        <div class="col-md-6">

                                        <div class="form-group">
                                            <label>Calling Minutes</label>
                                            <input type="number" name="calling" class="form-control" required placeholder="Please enter calling minutes" value="{{old('calling') ?? $plan->calling}}" />
                                            @error('calling')
                                            <span class="invalid-feedback" role="alert">
                                                {{ $errors->first('calling') }}
                                            </span>
                                            @enderror
                                        </div>
                                        </div>
                                        <div class="col-md-6">

                                        <div class="form-group">
                                            <label>Data GB</label>
                                            <input type="number" name="data" class="form-control" required placeholder="Please Data in GB" value="{{old('data') ?? $plan->data}}" />
                                            @error('data')
                                            <span class="invalid-feedback" role="alert">
                                                {{ $errors->first('data') }}
                                            </span>
                                            @enderror
                                        </div>
                                        </div>
                                        </div>
                                   

                            </div>
                           

                        </div>
                        
                    </div>
                    <div class="box-footer">
                        <a href="{{ url('admin/planMobile/list') }}" class="btn btn-danger">&larr; Cancel</a> <button type="submit" class="btn btn-primary pull-right">Submit</button>
                    </div>
                </div>
            </form>
        </section>
    </div>
</div>

@endsection
