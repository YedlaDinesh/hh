@extends('layouts.user')
@section('content')
<section class="content-header">
    <h1>Managing Wifi Plans</h1>
    @if(\Session::has('success'))
    <br />
    <div class="alert alert-success">
        {{\Session::get('success')}}
    </div>
    @endif
    @if(\Session::has('danger'))
    <br />
    <div class="alert alert-danger">
        {{\Session::get('danger')}}
    </div>
    @endif
</section>
<section class="content">
    <form role="form" method="post" action="{{url('admin/plan/listaction')}}" id="list_client_form">
        <input type="hidden" value="{{csrf_token()}}" name="_token" />
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Plans</h3>
                <!-- <a href="{{ url('plan/addedit/0') }}" class="btn btn-warning pull-right">Add New</a> -->
            </div><!-- /.box-header -->
            <div class="box-body">
                <table id="tadblelist" class="table table-bordered table-striped" width="100%">
                    <thead>
                        <tr>
                            <th>Sr. No</th>
                            <th>Name</th>
                            <th>Price $</th>
                            <th>Data GB</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if ($plans->count())
                        <?php $i = 1;?>
                        @foreach($plans as $client)
                        <tr>
                            <td><?php echo $i; ?></td>
                            <td>{{$client->name}}</td>
                            <td>{{$client->price}}</td>
                            <td>{{$client->data}}</td>
                            <td><a href="{{ url('admin/plan/addeditWifi/'.$client->id) }}" class="btn btn-primary">Edit</a></td>
                            <?php $i++; ?>
                        </tr>

                        @endforeach
                        @else
                        <tr>
                            <td class="text-center" colspan="7">-no plans records-</td>
                        </tr>
                        @endif
                    </tbody>
                   
                </table>

            </div><!-- /.box-body -->
           
    </form>
</section>
@endsection

@section('javascript')
@if ($plans->count())
<script type="text/javascript">

    $(document).ready(function () {
        $("#tablelist").dataTable();

        $('.alert').fadeOut(4000);

        function validatechk()
        {
            
            var status = false;

            if ($('.delids:checkbox:checked').length == '0') {
                $.alert({
                    title: 'Alert!',
                    content: 'Please select atleast one!',
                });
                status = false;
                return status;
            }

            if (confirm("Are you sure to DELETE?")) {
                status = true;
                return status;
            }
            return status;
        }


        $('#list_client_form').submit(validatechk);


//        $('#sd').submit(function (e) {
//            e.preventDefault();
//
//
//
//            $.confirm({
//                title: 'Confirm!',
//                content: 'Simple confirm!',
//                buttons: {
//                    confirm: function () {
//                        $.alert('Confirmed!');
//                    },
//                    cancel: function () {
//                        $.alert('Canceled!');
//                    },
//
//                }
//            });
//        });


    });
</script>
@endif
@endsection
