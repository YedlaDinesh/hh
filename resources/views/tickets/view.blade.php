@extends('layouts.user')

@section('content')

<div class="row">
    <div class="col-md-9 col-center">
        <section class="content-header">
            <h1>Ticket Details </h1>
            @if(\Session::has('danger'))
            <br />
            <div class="alert alert-danger">
                {{\Session::get('danger')}}
            </div>
            @endif
        </section>
        <section class="content">
            <form role="form" method="post" action="" id="waitform">
                <!-- general form elements disabled -->
                <div class="box">
                    <div class="box-header">
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">

                                <div class="row">
                                    <!-- text input -->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Name</label>
                                            <input type="hidden" value="{{csrf_token()}}" name="_token" />
                                            <input type="text" name="name" class="form-control" disabled
                                                placeholder="Please enter Name"
                                                value="{{old('name') ?? $ticket->name}}" />

                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Price</label>
                                            <input type="hidden" value="{{csrf_token()}}" name="_token" />
                                            <input type="text" name="price" class="form-control" disabled
                                                placeholder="Please enter Price"
                                                value="{{old('price') ?? $ticket->email}}" />

                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <!-- text input -->

                                    <div class="col-md-12">

                                        <div class="form-group">
                                            <label>Query</label>
                                            <textarea class="form-control" name="query" cols="50" rows="10"
                                                value="{{old('query') ?? $ticket->query}}" disabled
                                                id="placeOfDeath">{{$ticket->query}}</textarea>

                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <!-- text input -->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>OrderId</label>
                                            <input type="hidden" value="{{csrf_token()}}" name="_token" />
                                            <input type="text" name="name" class="form-control" disabled
                                                placeholder="Please enter Name"
                                                value="{{old('orderId') ?? $ticket->orderId}}" />

                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Status</label>
                                            <input type="hidden" value="{{csrf_token()}}" name="_token" />
                                            <select class="form-control" name="status">
                                                <option disabled> Choose Status</option>
                                                <option <?php if($ticket->status == 0){ echo 'selected';} ?> value="0">
                                                    Pending</option>
                                                <option <?php if($ticket->status == 1){ echo 'selected';} ?> value="1">
                                                    Resolved</option>

                                            </select>

                                        </div>
                                    </div>
                                </div>


                            </div>


                        </div>

                    </div>
                    <div class="box-footer">
                        <a href="{{ url('admin/ticket/list') }}" class="btn btn-danger">&larr; Cancel</a> <button
                            type="submit" class="btn btn-primary pull-right">Submit</button>
                    </div>
                </div>
            </form>
        </section>
    </div>
</div>

@endsection