@extends('layouts.user')
@section('content')
<div class="row">
    <div class="col-md-9 col-center">
        <section class="content-header">
            <h1>Add SIM </h1>
            @if(\Session::has('danger'))
            <br />
            <div class="alert alert-danger">
                {{\Session::get('danger')}}
            </div>
            @endif
        </section>
        <section class="content">
            <form role="form" method="post" action="" id="waitform" autocomplete="off">
                <!-- general form elements disabled -->
                <div class="box">
                    <div class="box-header">
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-12">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>Contact Number</label>
                                            <input type="hidden" value="{{csrf_token()}}" name="_token" />
                                            <input type="text" name="sim_no" class="form-control" placeholder="Please enter sim number" value="{{old('sim_no') ?? $sim->sim_no}}" required />
                                            @error('sim_no')
                                            <span class="invalid-feedback" role="alert">
                                                {{ $errors->first('sim_no') }}
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label>Sim Id</label>
                                            <input type="text" name="sim_id" class="form-control" placeholder="Please enter sim Id" value="{{old('sim_id') ?? $sim->sim_id}}" required />
                                            @error('sim_id')
                                            <span class="invalid-feedback" role="alert">
                                                {{ $errors->first('sim_id') }}
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label>End Date</label>
                                            <input type="hidden" value="{{csrf_token()}}" name="_token" />
                                            <input type="text" name="end_date" class="form-control" placeholder="DD/MM/YYYY" id="end_date" value="{{old('end_date') ?? $sim->end_date}}"  autocomplete="off"/>
                                            @error('end_date')
                                            <span class="invalid-feedback" role="alert">
                                                {{ $errors->first('end_date') }}
                                            </span>
                                            @enderror
                                        </div>
                                       
                                        <div class="form-group">
                                            <label>Sim Type</label>
                                            <select class="form-control" name="sim_type"  id="sim_type">
                                                <option value="">Select sim type</option>
                                            @foreach(config('constant.SIM_TYPE') as $key => $simType)
                                                <option value="{{ $key }}" {{$sim->sim_type == $key ? 'selected' : ''}}>{{ $simType }}</option>
                                            @endforeach
                                            </select>
                                            @error('plan_id')
                                            <span class="invalid-feedback" role="alert">
                                                {{ $errors->first('plan_id') }}
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Sim ICCID</label>
                                            <input type="hidden" value="{{csrf_token()}}" name="_token" />
                                            <input type="text" name="sim_iccid" class="form-control" placeholder="Please enter ICCID number" value="{{old('sim_iccid') ?? $sim->sim_iccid}}" required />
                                            @error('sim_iccid')
                                            <span class="invalid-feedback" role="alert">
                                                {{ $errors->first('sim_iccid') }}
                                            </span>
                                            @enderror
                                        </div>
                                         
                                        <div class="form-group">
                                            <label>Start Date</label>
                                            <input type="hidden" value="{{csrf_token()}}" name="_token" />
                                            <input type="text" name="start_date" id="start_date" class="form-control" placeholder="DD/MM/YYYY" value="{{old('start_date') ?? $sim->start_date}}"  autocomplete="off" />
                                            @error('start_date')
                                            <span class="invalid-feedback" role="alert">
                                                {{ $errors->first('start_date') }}
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label>Sim Status </label>
                                            <select class="form-control" name="sim_status"  id="sim_status">
                                                <option value="">Select sim status</option>
                                                @foreach(config('constant.SIM_STATUS') as $key => $simStatus)
                                                    <option value="{{ $key }}" {{$sim->sim_status == $key ? 'selected' : ''}}>{{ $simStatus }}</option>
                                                @endforeach
                                            </select>
                                            @error('sim_status')
                                            <span class="invalid-feedback" role="alert">
                                                {{ $errors->first('sim_status') }}
                                            </span>
                                            @enderror
                                        </div>
                                         <div class="form-group">
                                            <label>End Date</label>
                                            <input type="hidden" value="{{csrf_token()}}" name="_token" />
                                            <input type="text" name="end_date" class="form-control" placeholder="DD/MM/YYYY" id="end_date" value="{{old('end_date') ?? $sim->end_date}}"  autocomplete="off"/>
                                            @error('end_date')
                                            <span class="invalid-feedback" role="alert">
                                                {{ $errors->first('end_date') }}
                                            </span>
                                            @enderror
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <a href="{{ url('admin/sim/list') }}" class="btn btn-danger">&larr; Cancel</a> <button type="submit" class="btn btn-primary pull-right">Submit</button>
                    </div>
                </div>
            </form>
        </section>
    </div>
</div>
@endsection
@section('javascript')
<!-- Date-picker-css -->
<link rel="stylesheet" href="{{ asset('/css/bootstrap-datepicker.min.css') }}">
<!-- Date-picker-js -->
<script src="{{ asset('js/bootstrap-datepicker.min.js') }}"></script>

<script type="text/javascript">
    $('#start_date').datepicker({
        todayHighlight: true,
        format: "dd/mm/yyyy",
        autoclose: true,
    });
    $('#end_date').datepicker({
        todayHighlight: true,
        format: "dd/mm/yyyy",
        autoclose: true,
    });

</script>

@endsection