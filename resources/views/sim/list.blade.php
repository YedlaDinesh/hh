@extends('layouts.user')
@section('content')
<section class="content-header">
    <h1>Manage Sim</h1>
    @if(\Session::has('success'))
    <br />
    <div class="alert alert-success">
        {{\Session::get('success')}}
    </div>
    @endif
    @if(\Session::has('danger'))
    <br />
    <div class="alert alert-danger">
        {{\Session::get('danger')}}
    </div>
    @endif
</section>
<section class="content">
    <form role="form" method="post" action="{{url('admin/sim/listaction')}}" id="list_client_form">
        <input type="hidden" value="{{csrf_token()}}" name="_token" />
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Sim List</h3>
                <a href="{{ url('admin/sim/addedit/0') }}" class="btn btn-warning pull-right">Add New</a>
            </div><!-- /.box-header -->
            <div class="box-body">
                <table id="tablelist" class="table table-bordered table-striped" width="100%">
                    <thead>
                        <tr>
                            <th class="text-center" width="5%"><input type="checkbox" name="chktgl" title="Select/Unselect All" class="seldel"></th>
                            <th>Sim Number</th>
                            <th>Sim Type</th>
                            <th>Sim Id</th>
                            <th>Sim ICCID</th>
                            <th>Sim status</th>
                            <th>Plan no</th>
                            <th>Start date</th>
                            <th>End date</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if ($sims->count())
                        @foreach($sims as $i => $sim)
                        <tr>
                            <td class="text-center"><input type="checkbox" name="del[]" value="{{$sim->id}}" class="delids"></td>
                            <td>{{$i+1}}</td>
                            <td>{{$sim->sim_no}}</td>
                            <td>{{ ($sim->sim_type == 1) ? 'Class 1' : (($sim->sim_type == 2) ? 'Class 2' : 'Prefer not to say') }}</td>
                            <td>{{$sim->sim_id}}</td>
                            <td>{{$sim->sim_iccid}}</td>
                            <td>
                                {{ ($sim->sim_status == 1) ? "Yes": (($wifi->app_loaded == 2) ? "No": "Prefer not to say") }}
                            </td>
                            <td>{{$sim->plan_id}}</td>
                            <td>{{$sim->start_date}}</td>
                            <td>{{$sim->end_date}}</td>
                            
                            <td>
                                <a href="{{ url('admin/sim/addedit/'.$sim->id) }}" class="btn btn-primary">Edit</a>&nbsp&nbsp
                            </td>
                        </tr>
                        @endforeach
                        @else
                        <tr>
                            <td class="text-center" colspan="7">-No sim found-</td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div><!-- /.box-body -->
            @if ($sims->count())
            <div class="box-footer">
                <button type="submit" class="btn btn-danger">Delete</button>
            </div>
            @endif
    </form>
</section>
@endsection
@section('javascript')
@if ($sims->count())
<script type="text/javascript">
$(document).ready(function() {
    $("#tablelist").dataTable();

    $('.alert').fadeOut(4000);

    function validatechk() {

        var status = false;

        if ($('.delids:checkbox:checked').length == '0') {
            $.alert({
                title: 'Alert!',
                content: 'Please select atleast one!',
            });
            status = false;
            return status;
        }

        if (confirm("Are you sure to DELETE?")) {
            status = true;
            return status;
        }
        return status;
    }


    $('#list_client_form').submit(validatechk);

});

</script>
@endif
@endsection
