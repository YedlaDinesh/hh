<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>||HH||</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=PT+Sans:wght@400;700&family=Raleway:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
    <link href="{{ asset('/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{ asset('/fonts/stylesheet.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/animate.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/responsive.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/owl.carousel.min.css') }}">
</head>

<body>
    <!--heaader-main-->
    <header>
        <div class="top-bar">
            <div class="container">
                <div class="row">
                    <div class="col-sm-7">
                        <div class="contact-top">
                            <div class="left-contact">
                                <img src="{{ asset('/images/phone.png') }}" alt="">
                                <p>1234567890 <span>/</span> 1234567890</p>
                            </div>
                            <div class="right-contact">
                                <img src="{{ asset('/images/time.png') }}" alt="">
                                <p>24 hrs. Service</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-5">
                        <div class="get-ticket">
                            <a href="{{ url('login') }}"><img src="{{ asset('/images/login-btn.png') }}" alt=""></a>
                            <a href="{{ url('create-ticket') }}"><img src="{{ asset('/images/ticket.png') }}" alt=""></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="main-nav-bar-top">
            <div class="container">
                <nav class="navbar navbar-expand-lg navbar-light bg-light">
                    <a class="navbar-brand" href="{{ url('/') }}">HXXXXXXXX</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item">
                                <a class="nav-link active" href="{{ url('/') }}">Home</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ url('aboutus') }}">About Us</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ url('plan-pricing') }}">Plans & Pricing</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ url('reviews') }}">Reviews</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ url('blogs') }}">Blogs</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ url('faq') }}">FAQ</a>
                            </li>
                        </ul>
                    </div>
                    <div class="side-toggle">
                        <div id="mySidenav" class="sidenav">
                            <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
                            <a href="{{ url('aboutus') }}">About</a>
                            <a href="#">Services</a>
                            <a href="#">Clients</a>
                            <a href="#">Contact</a>
                        </div>
                        <span style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776;</span>
                    </div>
                </nav>
            </div>
        </div>
    </header>

    <div id="app">
        <main class="py-4">
            @yield('content')
        </main>
    </div>
    <footer class="footer-main block-element">
    <div class="loaction">
        <div class="row">
            <div class="col-sm-4">
                <div class="loaction-inner">
                    <h6>Location</h6>
                    <p>HH<br>
                        1 Vashist road, Manali,<br>
                        Himachal Pradesh 175103</p>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="loaction-inner">
                    <h6>Phone</h6>
                    <p>Tel.: +91 123456789<br>
                    support@hh.com<br>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="loaction-inner">
                    <h6>Hour</h6>
                    <p>Check in 12: 00<br>
                    Check out 12:00<br>
                    Online 24/7
                </div>
            </div>
        </div>
    </div>
    <div class="footer-nav">
        <ul class="fitr-fst-menu">
            <li><a href="{{ url('partner-with-us') }}">Partner with us</a></li>
            <li><a href="{{ url('refer-earn') }}">Refer & Earn</a></li>
            <li><a href="#">Write for Us</a></li>
            <li><a href="#">Help</a></li>
        </ul>
        <ul class="fitr-sec-menu">
            <li><a href="{{ url('aboutus') }}">About</a></li>
            <li><a href="{{ url('blogs') }}">Blog Cloud</a></li>
            <li><a href="{{ url('faq') }}">FAQ</a></li>
            <li><a href="#">Terms & Conditions</a></li>
            <li><a href="#">Contact Us</a></li>
            <li><a href="#">Site Map</a></li>
            <li><a href="#">Cookies Policy</a></li>
        </ul>
    </div>
    </footer>
    <!--js-->
    <!-- jQuery 2.1.4 -->
    <!-- <script src="{{ asset('/js/jquery.min.js') }}"></script> -->
    <script src="{{ asset('/plugins/jQuery/jQuery-2.1.4.min.js') }}"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="{{ asset('js/bootstrap.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/js/owl.carousel.js') }}"></script>
    <script src="{{ asset('/js/custom.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.js"></script>
    @yield('javascript')
    <script>
    function openNav() {
        document.getElementById("mySidenav").style.width = "250px";
    }
    function closeNav() {
        document.getElementById("mySidenav").style.width = "0";
    }
    $(document).ready(function() {
        $(window).scroll(function(){
            if ($(this).scrollTop() > 60) {
                $('body').addClass('fixed');
            } else {
                $('body').removeClass('fixed');
            }
        });
    });
    </script>
</body>
</html>