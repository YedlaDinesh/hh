<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="UTF-8">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.4 -->
    <link href="{{ asset('/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet"
        type="text/css" />
    <!-- Ionicons -->
    <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="{{ asset('/dist/css/AdminLTE.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/dist/css/skins/skin-yellow.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    @yield('header')
</head>
<!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->

<body class="skin-yellow layout-top-nav">
    <div class="wrapper">
        <header class="main-header">
            <nav class="navbar navbar-static-top">
                <div class="container">
                    <div class="navbar-header">
                        <a href="{{ url('admin/home')}}" class="navbar-brand"><b>HH</b>App</a>
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#navbar-collapse">
                            <i class="fa fa-bars"></i>
                        </button>
                    </div>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse pull-right" id="navbar-collapse">
                        <ul class="nav navbar-nav">
                            <!-- <li class="active"><a href="{{url('#')}}"> Orders <span class="sr-only">(current)</span></a></li>-->

                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Blog Section <span
                                        class="caret"></span></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="{{ url('admin/blog/list') }}">Blogs</a></li>
                                    <li class="divider"></li>
                                    <li><a href="{{ url('admin/category/list') }}">Categories</a></li>
                                </ul>
                            </li>
                            <li><a href="{{ url('admin/faq/list') }}">Faqs</a></li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Plans <span
                                        class="caret"></span></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="{{ url('admin/planMobile/list') }}">Mobile Plans</a></li>
                                    <li class="divider"></li>
                                    <li><a href="{{ url('admin/planWifi/list') }}">Wifi Plans</a></li>
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Devices <span
                                        class="caret"></span></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="{{ url('admin/device/list') }}">Devices</a></li>
                                    <li class="divider"></li>
                                    <li><a href="{{ url('admin/sim/list') }}">Sim</a></li>
                                    <li class="divider"></li>
                                    <li><a href="{{ url('admin/accessorie/list') }}">Accessories</a></li>
                                </ul>
                            </li>
                            <li class="active"><a href="{{url('admin/ticket/list')}}"> Tickets <span
                                        class="sr-only">(current)</span></a></li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Manage Orders <span
                                        class="caret"></span></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="{{ url('admin/order/list') }}">Orders</a></li>
                                    <li class="divider"></li>
                                    <li><a href="{{ url('admin/customer/list') }}">Customers</a></li>
                                    <li class="divider"></li>
                                    <li><a href="{{ url('admin/payments/list') }}">Payment</a></li>
                                    <li class="divider"></li>
                                    <li><a href="{{ url('admin/leads/list') }}">Leads</a></li>
                                </ul>
                            </li>

                            <!-- <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Logistics <span class="caret"></span></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="{{ url('admin/order/list') }}">Orders</a></li>
                                    <li class="divider"></li>
                                    <li><a href="{{ url('admin/customer/list') }}">Customers</a></li>
                                </ul>
                            </li>
                            <!-- <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Settings <span class="caret"></span></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="{{ url('#') }}">Payment Gateways</a></li>
                                    <li class="divider"></li>
                                    <li><a href="{{ url('#') }}">Application Settings</a></li>
                                    <li class="divider"></li>
                                    <li><a href="{{ url('#') }}">Tickets</a></li>
                                    <li class="divider"></li>
                                    <li><a href="{{ url('#') }}">Announcements</a></li>
                                </ul>
                            </li> -->
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Hi, {{ Auth::user()->name }}
                                    <span class="caret"></span></a>
                                <ul class="dropdown-menu" role="menu">
                                    <!-- <li><a href="{{ url('#') }}">Profile</a></li> -->
                                    <!--<li class="divider"></li>-->
                                    <li><a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            {{ __('Logout') }}
                                        </a>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                            style="display: none;">
                                            @csrf
                                        </form>
                                    </li>
                                </ul>
                            </li>
                            <!-- User Account Menu -->
                        </ul>
                    </div><!-- /.navbar-custom-menu -->
                </div><!-- /.container-fluid -->
            </nav>
        </header>
        <!-- Full Width Column -->
        <div class="content-wrapper">
            <div class="container">
                @yield('content')
            </div><!-- /.container -->
        </div><!-- /.content-wrapper -->
        <footer class="main-footer">
            <div class="container">
                <div class="pull-right hidden-xs">
                    <b>Version</b> 0.1
                </div>
                <strong>Copyright &copy; 2020 <a href="#">HH.com</a>.</strong> All rights reserved.
            </div><!-- /.container -->
        </footer>
    </div><!-- ./wrapper -->
    <!-- jQuery 2.1.4 -->
    <script src="{{ asset('/plugins/jQuery/jQuery-2.1.4.min.js') }}"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="{{ asset('/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
    <!-- SlimScroll -->
    <script src="{{ asset('/plugins/slimScroll/jquery.slimscroll.min.js') }}" type="text/javascript"></script>
    <!-- FastClick -->
    <script src="{{ asset('/plugins/fastclick/fastclick.min.js') }}"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset('/dist/js/app.min.js') }}" type="text/javascript"></script>
    <!-- Datatable -->
    <script src="{{ asset('/plugins/datatables/jquery.dataTables.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/plugins/datatables/dataTables.bootstrap.min.js') }}" type="text/javascript"></script>
<<<<<<< HEAD

=======
>>>>>>> d1aa5e043022b88ba362816d31417ec0e7e87cde
    <!-- Custom -->
    <script src="{{ asset('/dist/js/custom.js') }}" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
    @yield('javascript')
</body>

</html>