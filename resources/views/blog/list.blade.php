@extends('layouts.user')

@section('content')
<section class="content-header">
    <h1>Manage Blogs</h1>
    @if(\Session::has('success'))
    <br />
    <div class="alert alert-success">
        {{\Session::get('success')}}
    </div>
    @endif
    @if(\Session::has('danger'))
    <br />
    <div class="alert alert-danger">
        {{\Session::get('danger')}}
    </div>
    @endif
</section>
<section class="content">
    <form role="form" method="post" action="{{url('admin/blog/listaction')}}" id="list_client_form">
        <input type="hidden" value="{{csrf_token()}}" name="_token" />
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Blogs List</h3>
                <a href="{{ url('admin/blog/addedit/0') }}" class="btn btn-warning pull-right">Add new blog</a>
            </div><!-- /.box-header -->
            <div class="box-body">
                <table id="tablelist" class="table table-bordered table-striped" width="100%">
                    <thead>
                        <tr>
                            <th class="text-center" width="5%"><input type="checkbox" name="chktgl" title="Select/Unselect All" class="seldel"></th>
                            <th>S.no</th>
                            <th>Title</th>
                            <th>Category</th>
                            <th>Published</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if ($blogs->count())
                        @foreach($blogs as $i => $blog)
                        <tr>
                            <td class="text-center"><input type="checkbox" name="del[]" value="{{$blog->id}}" class="delids"></td>
                            <td>{{$i+1}}</td>
                            <td>{{$blog->title}}</td>
                            <td>{{$blog->category->category_name}}</td>
                            <td>
                                {{ ($blog->is_published == 1) ? "Published": "Not published" }}
                            </td>
                            <td>
                                <a href="{{ url('admin/blog/addedit/'.$blog->id) }}" class="btn btn-primary">Edit</a>&nbsp&nbsp
                            </td>
                        </tr>
                        @endforeach
                        @else
                        <tr>
                            <td class="text-center" colspan="7">-No blogs found-</td>
                        </tr>
                        @endif
                    </tbody>
                    
                </table>

            </div><!-- /.box-body -->
            @if ($blogs->count())
            <div class="box-footer">
                <button type="submit" class="btn btn-danger">Delete</button>
            </div>
            @endif
    </form>
</section>
@endsection

@section('javascript')
@if ($blogs->count())
<script type="text/javascript">

    $(document).ready(function () {
        $("#tablelist").dataTable();

        $('.alert').fadeOut(4000);

        function validatechk()
        {
            
            var status = false;

            if ($('.delids:checkbox:checked').length == '0') {
                $.alert({
                    title: 'Alert!',
                    content: 'Please select atleast one!',
                });
                status = false;
                return status;
            }

            if (confirm("Are you sure to DELETE?")) {
                status = true;
                return status;
            }
            return status;
        }


        $('#list_client_form').submit(validatechk);


//        $('#sd').submit(function (e) {
//            e.preventDefault();
//
//
//
//            $.confirm({
//                title: 'Confirm!',
//                content: 'Simple confirm!',
//                buttons: {
//                    confirm: function () {
//                        $.alert('Confirmed!');
//                    },
//                    cancel: function () {
//                        $.alert('Canceled!');
//                    },
//
//                }
//            });
//        });


    });
</script>
@endif
@endsection