@extends('layouts.user')
@section('content')
<div class="row">
    <div class="col-md-9 col-center">
        <section class="content-header">
            <h1>Add Blog </h1>
            @if(\Session::has('danger'))
            <br />
            <div class="alert alert-danger">
                {{\Session::get('danger')}}
            </div>
            @endif
        </section>
        <section class="content">
            <form role="form" method="post" action="" id="waitform" enctype="multipart/form-data">
                <!-- general form elements disabled -->
                <div class="box">
                    <div class="box-header">
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-12">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>Title</label>
                                            <input type="hidden" value="{{csrf_token()}}" name="_token" />
                                            <input type="text" name="title" class="form-control" placeholder="Please enter blog title" value="{{old('title') ?? $blog->title}}" required id="title" />
                                            @error('title')
                                            <span class="invalid-feedback" role="alert">
                                                {{ $errors->first('title') }}
                                            </span>
                                            @enderror
                                        </div>
                                      
                                        <div class="form-group">
                                            <label>Published</label>
                                            <select class="form-control" name="is_published" required id="is_published">
                                                @foreach(config('constant.IS_PUBLISHED') as $key => $pub)
                                                    <option value="{{ $key }}" {{$blog->is_published == $key ? 'selected' : ''}}>{{ $pub }}</option>
                                                @endforeach
                                            </select>
                                            @error('is_published')
                                            <span class="invalid-feedback" role="alert">
                                                {{ $errors->first('is_published') }}
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label>Image</label>
                                            <input type="hidden" value="{{csrf_token()}}" name="_token" />
                                            <input type="file" name="image" class="form-control"  accept="image/gif, image/jpeg, image/png" />
                                            @error('image')
                                            <span class="invalid-feedback" role="alert">
                                                {{ $errors->first('image') }}
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <div id="imagePreview">
                                                <img id="priview" src="{{ old('image') ?? $blog->image ? URL::to('/').'/images/blogs/thumbnail/'.$blog->image : '#'}}" alt="Blog image" style="max-height: 50%;max-width: 50%;" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-12">
                                    <div class="form-group">
                                            <label>Slug </label>
                                            <input type="text" name="slug" class="form-control" placeholder="Please enter slug" value="{{old('slug') ?? $blog->slug}}" required id="slug" />
                                            @error('app_loaded')
                                            <span class="invalid-feedback" role="alert">
                                                {{ $errors->first('slug') }}
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label>Category</label>
                                            <select class="form-control" name="cat_id" required id="cat_id">
                                            <option>Select category</option>
                                                @foreach($categories as $key => $cat)
                                                    <option value="{{ $key }}" {{$blog->cat_id == $key ? 'selected' : ''}}>{{ $cat }}</option>
                                                @endforeach
                                            </select>
                                            @error('cat_id')
                                            <span class="invalid-feedback" role="alert">
                                                {{ $errors->first('cat_id') }}
                                            </span>
                                            @enderror
                                        </div>
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">    
                                    <label>Description</label>
                                    <textarea class="form-control" id="description" name="description" rows='8'>{{old('description') ?? $blog->description}}</textarea>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <a href="{{ url('admin/blog/list') }}" class="btn btn-danger">&larr; Cancel</a> <button type="submit" class="btn btn-primary pull-right">Submit</button>
                    </div>
                </div>
            </form>
        </section>
    </div>
</div>
@endsection
@section('javascript')
<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>
<script>
$(document).ready(function() {
  $('#description').summernote({
    height: 250,
    minHeight: null,
    maxHeight: null
  });
  
});

$('#title').on('change',function(e) {
    $.get('{{ url('admin/blog/check_slug') }}', 
      { 'title': $(this).val() }, 
      function( data ) {
        $('#slug').val(data.slug);
      }
    );
  });

//----------------------------Blog_image_preview-----------------
var imgSrc=$('#priview').attr('src');
if (imgSrc) {
    $('#priview').show();
}
if (imgSrc == '#' || imgSrc == '../') {
    $('#priview').hide();
}

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            $('#priview').attr('src', e.target.result);
            $('#priview').show();
        }
        reader.readAsDataURL(input.files[0]); 
    }
}

$("input[name='image']").change(function() {
    readURL(this);
});

</script>
@endsection