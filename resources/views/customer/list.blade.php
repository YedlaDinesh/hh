@extends('layouts.user')

@section('content')
<section class="content-header">
    <h1>Manage customers </h1>
    @if(\Session::has('success'))
    <br />
    <div class="alert alert-success">
        {{\Session::get('success')}}
    </div>
    @endif
    @if(\Session::has('danger'))
    <br />
    <div class="alert alert-danger">
        {{\Session::get('danger')}}
    </div>
    @endif
</section>
<section class="content">
    <form role="form" method="post" action="{{url('admin/order/listaction')}}" id="list_client_form">
        <input type="hidden" value="{{csrf_token()}}" name="_token" />
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Customer list</h3>
                <!-- <a href="{{ url('plan/addedit/0') }}" class="btn btn-warning pull-right">Add New</a> -->
            </div><!-- /.box-header -->
            <div class="box-body">
                <table id="tablelist" class="table table-bordered table-striped" width="100%">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Customer name</th>
                            <th>Order no</th>
                            <th>Email</th>
                            <th>DOB</th>
                            <th>Country</th>
                            <th>Payment status</th>
                            <th>View</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i = 1;?>
                        @if ($customers->count())
                        @foreach($customers as $customer)
                        <tr>
                            <td>{{$i++}}</td>
                            <td>{{$customer->fullName}}</td>
                            <td>{{$customer->bookForeignId}}</td>
                            <td>{{$customer->customerEmail}}</td>
                            <td></td>
                            <td>{{$customer->country}}</td>
                            <td> {{ $customer->status == 1 ? "Provisioned": "No" }}</td>
                            <td><a href="{{ url('admin/customer/detail/'.$customer->bookForeignId) }}"
                                    class="btn btn-primary">View customer</a></td>
                        </tr>
                        @endforeach
                        @else
                        <tr>
                            <td class="text-center" colspan="7">-no orders -</td>
                        </tr>
                        @endif
                    </tbody>

                </table>

            </div><!-- /.box-body -->

    </form>
</section>
@endsection

@section('javascript')
@if ($customers->count())
<script type="text/javascript">
$(document).ready(function() {
    $("#tablelist").dataTable();

    $('.alert').fadeOut(4000);

    function validatechk() {

        var status = false;

        if ($('.delids:checkbox:checked').length == '0') {
            $.alert({
                title: 'Alert!',
                content: 'Please select atleast one!',
            });
            status = false;
            return status;
        }

        if (confirm("Are you sure to DELETE?")) {
            status = true;
            return status;
        }
        return status;
    }


    $('#list_client_form').submit(validatechk);
});
</script>
@endif
@endsection