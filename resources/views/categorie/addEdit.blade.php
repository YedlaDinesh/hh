@extends('layouts.user')
@section('content')
<div class="row">
    <div class="col-md-9 col-center">
        <section class="content-header">
            <h1>Add Categorie </h1>
            @if(\Session::has('danger'))
            <br />
            <div class="alert alert-danger">
                {{\Session::get('danger')}}
            </div>
            @endif
        </section>
        <section class="content">
            <form role="form" method="post" action="" id="waitform" enctype="multipart/form-data">
                <!-- general form elements disabled -->
                <div class="box">
                    <div class="box-header">
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-6">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>Categorie Name</label>
                                            <input type="hidden" value="{{csrf_token()}}" name="_token" />
                                            <input type="text" name="category_name" class="form-control" placeholder="Please enter categorie name" value="{{old('category_name') ?? $categorie->category_name}}" required />
                                            @error('category_name')
                                            <span class="invalid-feedback" role="alert">
                                                {{ $errors->first('category_name') }}
                                            </span>
                                            @enderror
                                        </div>
                                        </div>
                                        <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Image</label>
                                            <input type="hidden" value="{{csrf_token()}}" name="_token" />
                                            <input type="file" name="image" class="form-control"  accept="image/gif, image/jpeg, image/png" />
                                            @error('image')
                                            <span class="invalid-feedback" role="alert">
                                                {{ $errors->first('image') }}
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <div id="imagePreview">
                                                <img id="priview" src="{{ old('image') ?? $categorie->image ? URL::to('/').'/images/categories/'.$categorie->image : '#'}}" alt="Blog image" style="max-height: 50%;max-width: 50%;" />
                                            </div>
                                        </div>
                                        </div>
                                        <!-- <div class="col-md-6">

                                        <div class="form-group">
                                            <div id="imagePreview">
                                                <img id="priview" src="{{ old('image')}}" alt="Blog image" style="max-height: 50%;max-width: 50%;" />
                                            </div>
                                        </div>
                                        </div> -->
                                        
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <a href="{{ url('admin/category/list') }}" class="btn btn-danger">&larr; Cancel</a> <button type="submit" class="btn btn-primary pull-right">Submit</button>
                    </div>
                </div>
            </form>
        </section>
    </div>
</div>
@endsection
@section('javascript')
<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>
<script>
$(document).ready(function() {
 
 
  
});



//----------------------------Blog_image_preview-----------------
var imgSrc=$('#priview').attr('src');
if (imgSrc) {
    $('#priview').show();
}
if (imgSrc == '#' || imgSrc == '../') {
    $('#priview').hide();
}

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            $('#priview').attr('src', e.target.result);
            $('#priview').show();
        }
        reader.readAsDataURL(input.files[0]); 
    }
}

$("input[name='image']").change(function() {
    readURL(this);
});

</script>
@endsection
