@extends('layouts.user')

@section('content')
    <section class="content-header">
        <h1>Dashboard</h1>
    </section>
    <section class="content">
    <!---<div class="callout callout-info">
        <h4>Tip!</h4>
        <p>Start with adding your hosting details. <a>Click here</a> to configure.</p>
    </div>-->
    <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box bg-aqua">
                <span class="info-box-icon"><i class="fa fa-shopping-cart"></i></span>
                <div class="info-box-content"><br />
                  <span class="info-box-text">Outstanding Orders</span>
                  <span class="info-box-number">0</span>
                  
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div><!-- /.col -->
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box bg-green">
                <span class="info-box-icon"><i class="fa fa-comments-o"></i></span>
                <div class="info-box-content">
                  <br />
                  <span class="info-box-text">Open Tickets</span>
                  <span class="info-box-number">0</span>
                  
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div><!-- /.col -->
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box bg-yellow">
                <span class="info-box-icon"><i class="fa fa-exclamation-circle"></i></span>
                <div class="info-box-content">
                <br />
                  <span class="info-box-text">Cancellations</span>
                  <span class="info-box-number">0</span>
                  
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div><!-- /.col -->
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box bg-purple">
                <span class="info-box-icon"><i class="fa fa-server"></i></span>
                <div class="info-box-content">
                <br />
                  <span class="info-box-text">Total Accounts</span>
                  <span class="info-box-number">0</span>
                  
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div><!-- /.col -->
          </div>
    
    </section>
@endsection
