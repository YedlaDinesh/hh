@extends('layouts.user')

@section('content')

<div class="row">
    <div class="col-md-9 col-center">
        <section class="content-header">
            <h1>Order Details </h1>
            @if(\Session::has('danger'))
            <br />
            <div class="alert alert-danger">
                {{\Session::get('danger')}}
            </div>
            @endif
        </section>
        <section class="content">
            <form role="form" method="post" action="" id="waitform">
                <!-- general form elements disabled -->
                <div class="box">
                    <div class="box-header">
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">

                                <div class="row">
                                    <!-- text input -->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Order no</label>
                                            <input type="hidden" value="{{csrf_token()}}" name="_token" />
                                            <input type="text" name="name" class="form-control" disabled
                                                placeholder="Please enter Name"
                                                value="{{old('bookForeignId') ?? $orders->bookForeignId}}" />

                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Email</label>
                                            <input type="hidden" value="{{csrf_token()}}" name="_token" />
                                            <input type="text" name="price" class="form-control" disabled
                                                placeholder="Please enter Price"
                                                value="{{old('customerEmail') ?? $orders->customerEmail}}" />

                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <!-- text input -->

                                    <div class="col-md-12">

                                        <div class="form-group">
                                            <label>No. of Devices</label>
                                            <table class="table table-sm">
                                                <thead>
                                                    <tr>
                                                        <th scope="col">#</th>
                                                        <th scope="col">Device type</th>
                                                        <th scope="col">Device quantity</th>
                                                        <th scope="col">Device price</th>
                                                        <th scope="col">No. of days</th>
                                                        <th scope="col">Plan</th>
                                                        <th scope="col">Accessorie quantity</th>
                                                        <th scope="col">Accessorie price</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @php $i = 1; @endphp
                                                    @if ($orders->bookings->count())
                                                    @foreach($orders->bookings as $booking)
                                                    <tr>
                                                        <th scope="row">{{$i++}}</th>
                                                        <td>{{ ($booking->deviceId == 1) ? "Mobile": 'Wifi Hotspot' }}
                                                        </td>
                                                        <td>{{$booking->deviceQty}}</td>
                                                        <td>{{$booking->devicePrice}}</td>
                                                        <td>{{$booking->days}}</td>
                                                        <td>{{$booking->planId}}</td>
                                                        <td>{{$booking->accessorieQty ? $booking->accessorieQty : "Null"}}
                                                        </td>
                                                        <td>{{$booking->accessoriePrice ? $booking->accessoriePrice : "Null"}}
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                    @else
                                                    <tr>
                                                        <td class="text-center" colspan="8">-No device found -</td>
                                                    </tr>
                                                    @endif
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <!-- text input -->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Total amount</label>
                                            <input type="hidden" value="{{csrf_token()}}" name="_token" />
                                            <input type="text" name="price" class="form-control" disabled placeholder=""
                                                value="{{old('totalAmount') ?? $orders->totalAmount}}" />

                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Status</label>
                                            <input type="hidden" value="{{csrf_token()}}" name="_token" />
                                            <select class="form-control" name="status" required id="app_loaded">
                                                <option>Select status</option>
                                                @foreach(config('constant.ORDERS_STATUS') as $key => $status)
                                                <option value="{{ $key }}"
                                                    {{$orders->status == $key ? 'selected' : ''}}>{{ $status }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <a href="{{ url('admin/order/list') }}" class="btn btn-danger">&larr; Cancel</a> <button
                            type="submit" class="btn btn-primary pull-right">Submit</button>
                    </div>
                </div>
            </form>
        </section>
    </div>
</div>

@endsection