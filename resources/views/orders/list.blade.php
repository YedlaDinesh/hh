@extends('layouts.user')

@section('content')
<section class="content-header">
    <h1>Manage orders </h1>
    @if(\Session::has('success'))
    <br />
    <div class="alert alert-success">
        {{\Session::get('success')}}
    </div>
    @endif
    @if(\Session::has('danger'))
    <br />
    <div class="alert alert-danger">
        {{\Session::get('danger')}}
    </div>
    @endif
</section>
<section class="content">
    <form role="form" method="post" action="{{url('admin/order/listaction')}}" id="list_client_form">
        <input type="hidden" value="{{csrf_token()}}" name="_token" />
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Orders list</h3>
                <!-- <a href="{{ url('plan/addedit/0') }}" class="btn btn-warning pull-right">Add New</a> -->
            </div><!-- /.box-header -->
            <div class="box-body">
                <table id="tablelist" class="table table-bordered table-striped" >
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Order no.</th>
                            <th>Customer name</th>
                            <th>Email</th>
                            <th>Rental start</th>
                            <th>Rental end</th>
                            <th>Device type</th>
                            <th>status</th>
                            <th>View</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i = 1;?>
                        @if ($orders->count())
                        @foreach($orders as $order)
                        <tr>
                            <td>{{$i++}}</td>
                            <td>{{$order->bookForeignId}}</td>
                            <td>{{$order->fullName}}</td>
                            <td>{{$order->customerEmail}}</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td> {{ $order->status == 1 ? "Provisioned": "No" }}</td>
                            <td><a href="{{ url('admin/order/detail/'.$order->bookForeignId) }}"
                                    class="btn btn-primary">View order</a></td>
                        </tr>

                        @endforeach
                        @else
                        <tr>
                            <td class="text-center" colspan="7">-no orders -</td>
                        </tr>
                        @endif
                    </tbody>

                </table>

            </div><!-- /.box-body -->

    </form>
</section>
@endsection

@section('javascript')
@if ($orders->count())
<script type="text/javascript">
$(document).ready(function() {
    $("#tablelist").dataTable();

    $('.alert').fadeOut(4000);

    function validatechk() {

        var status = false;

        if ($('.delids:checkbox:checked').length == '0') {
            $.alert({
                title: 'Alert!',
                content: 'Please select atleast one!',
            });
            status = false;
            return status;
        }

        if (confirm("Are you sure to DELETE?")) {
            status = true;
            return status;
        }
        return status;
    }


    $('#list_client_form').submit(validatechk);
});
</script>
@endif
@endsection