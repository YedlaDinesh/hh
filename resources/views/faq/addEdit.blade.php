@extends('layouts.user')
@section('content')
<div class="row">
    <div class="col-md-9 col-center">
        <section class="content-header">
            <h1>Add Faq </h1>
            @if(\Session::has('danger'))
            <br />
            <div class="alert alert-danger">
                {{\Session::get('danger')}}
            </div>
            @endif
        </section>
        <section class="content">
            <form role="form" method="post" action="" id="waitform" enctype="multipart/form-data">
                <!-- general form elements disabled -->
                <div class="box">
                    
                    
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">

                                <div class="row">


                                    <div class="col-md-6">
                                
                                        <!-- text input -->
                                      
                                        <div class="form-group">
                                            <label>Question Name</label>
                                            <input type="hidden" value="{{csrf_token()}}" name="_token" />

                                            <input type="text" name="question" class="form-control" placeholder="Please enter faq question" value="{{old('question') ?? $faq->question}}" required />



                                            @error('question')
                                            <span class="invalid-feedback" role="alert">
                                                {{ $errors->first('question') }}
                                            </span>
                                            @enderror
                                        </div>
                                        </div>


                                        <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Categorie Name</label>
                                            <select class="form-control" name="category"  id="category">
                                                <option value="">Select category</option>
                                            @foreach(config('constant.FAQ_CATEGORY') as $key => $faqCategory)
                                                 <option value="{{ $key }}" {{$faq->category == $key ? 'selected' : ''}}>{{ $faqCategory }}</option>
                                            @endforeach
                                            </select>
                                            <!-- <input type="text" name="category" class="form-control" placeholder="Please enter faq question" value="{{old('category') ?? $faq->category}}" required /> -->
                                            @error('category') 
                                            <span class="invalid-feedback" role="alert">
                                                {{ $errors->first('category') }}
                                            </span>
                                            @enderror
                                        </div>
                                        </div>

                                        <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Answer Name</label>
                                        
                                            <textarea rows="8" type="text" name="answer" class="form-control" placeholder="Please enter faq question" required >{{old('answer') ?? $faq->answer}}</textarea>
                                           
                                            @error('answer')
                                            <span class="invalid-feedback" role="alert">
                                                {{ $errors->first('answer') }}
                                            </span>
                                            @enderror
                                        </div> 
                                        </div>


                                        </div>                                            
                                        </div>
                                </div>
                            </div>
                            
                    <div class="box-footer">
                        <a href="{{ url('admin/faq/list') }}" class="btn btn-danger">&larr; Cancel</a> 
                        <button type="submit" class="btn btn-primary pull-right">Submit</button>
                    </div>


                </div>
            </form>
        </section>
    </div>
</div>
@endsection
