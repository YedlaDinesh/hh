@extends('layouts.user')

@section('content')
<section class="content-header">
    <h1>Manage Faq</h1>
    @if(\Session::has('success'))
    <br />
    <div class="alert alert-success">
        {{\Session::get('success')}}
    </div>
    @endif
    @if(\Session::has('danger'))
    <br />
    <div class="alert alert-danger">
        {{\Session::get('danger')}}
    </div>
    @endif
</section>
<section class="content">
    <form role="form" method="post" action="{{url('admin/faq/listaction')}}" id="list_client_form">
        <input type="hidden" value="{{csrf_token()}}" name="_token" />
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Faqs List</h3>
                <a href="{{ url('admin/faq/addedit/0') }}" class="btn btn-warning pull-right">Add New</a>
            </div><!-- /.box-header -->
            <div class="box-body">
                <table id="tablelist" class="table table-bordered table-striped" width="100%">
                    <thead>
                        <tr>
                            <th class="text-center" width="5%"><input type="checkbox" name="chktgl" title="Select/Unselect All" class="seldel"></th>
                            <th width="5%" >S.no</th>
                            <th width="30%" >Question</th>
                            <th width="40%" >Answer</th>
                            <th width="15%" >Category</th>
                            <th width="5%" >Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if ($Faqs->count())
                        @foreach($Faqs as $i => $faq)
                        <tr>
                            <td class="text-center"><input type="checkbox" name="del[]" value="{{$faq->id}}" class="delids"></td>
                            <td>{{$i+1}}</td>
                            <td>{{$faq->question}}</td>
                            <td>{{$faq->answer}}</td>
                            <td>
                                            @foreach(config('constant.FAQ_CATEGORY') as $key => $faqCategory)
                                            {{$faq->category == $key ? $faqCategory : ''}}
                                            @endforeach
                            </td>
                            <td>
                                <a href="{{ url('admin/faq/addedit/'.$faq->id) }}" class="btn btn-primary">Edit</a>&nbsp&nbsp
                            </td>
                        </tr>
                        @endforeach
                        @else
                        <tr>
                            <td class="text-center" colspan="7">-No faq found-</td>
                        </tr>
                        @endif
                    </tbody>
                    
                </table>

            </div><!-- /.box-body -->
            @if ($Faqs->count())
            <div class="box-footer">
                <button type="submit" class="btn btn-danger">Delete</button>
            </div>
            @endif
    </form>
</section>
@endsection

@section('javascript')
@if ($Faqs->count())
<script type="text/javascript">

    $(document).ready(function () {
        $("#tablelist").dataTable();

        $('.alert').fadeOut(4000);

        function validatechk()
        {
            
            var status = false;

            if ($('.delids:checkbox:checked').length == '0') {
                $.alert({
                    title: 'Alert!',
                    content: 'Please select atleast one!',
                });
                status = false;
                return status;
            }

            if (confirm("Are you sure to DELETE?")) {
                status = true;
                return status;
            }
            return status;
        }


        $('#list_client_form').submit(validatechk);


//        $('#sd').submit(function (e) {
//            e.preventDefault();
//
//
//
//            $.confirm({
//                title: 'Confirm!',
//                content: 'Simple confirm!',
//                buttons: {
//                    confirm: function () {
//                        $.alert('Confirmed!');
//                    },
//                    cancel: function () {
//                        $.alert('Canceled!');
//                    },
//
//                }
//            });
//        });


    });
</script>
@endif
@endsection