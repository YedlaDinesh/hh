@extends('layouts.home')

@section('content')
<!--banner-section-->
<div class="banner block-element">
    <div class="home-demo">
        <div class="owl-carousel home-crousal">
            <div class="item">
                <img src="{{ asset('/images/taj-banner.jpg') }}" alt="">
                <div class="container">
                    <div class="over-lay-bnr">
                        <h1>Stay in contact with<br> homeland</h1>
                        <p>Rent Personal Phone & WI-FI hotspot while traveling india</p>
                        <div class="bnr-btn">
                            <a class="bl-btn" href="{{ url('plan-pricing#phone') }}">Book Phone</a>
                            <a class="bl-btn" href="{{ url('plan-pricing#wifi') }}">Book WI-FI hotspot</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <img src="{{ asset('/images/taj-banner.jpg') }}" alt="">
                <div class="container">
                    <div class="over-lay-bnr">
                        <h1>Stay in contact with<br> homeland</h1>
                        <p>Rent Personal Phone & WI-FI hotspot while traveling india</p>
                        <div class="bnr-btn">
                            <a class="bl-btn" href="{{ url('plan-pricing#phone') }}">Book Phone</a>
                            <a class="bl-btn" href="{{ url('plan-pricing#wifi') }}">Book WI-FI hotspot</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<section class="about-section block-element">
    <div class="container">
        <div class="row">
            <div class="col-sm-4">
                <h2>About Us</h2>
            </div>
            <div class="col-sm-8">
                <p>Looking for travel solutions? We are here to help you!</p>
            </div>
        </div>
    </div>
</section>
<section class="work-fr-you block-element">
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <h2>How It<br> <span>Works</span><br> For <span>You</span></h2>
            </div>
            <div class="col-sm-9">
                <div class="row">
                    <div class="col-sm-3 wow fadeInRight animate">
                        <div class="work-fr-you-in">
                            <h4>Book Your Plan Online</h4>
                            <div class="iner-pr">
                                <p>For phone Booking (You must be an E-Visa holder)</p>
                                <p>For Hotspot Booking (OCI/E-Visa/Regular Visa Holder can apply)</p>
                            </div>
                            <img src="{{ asset('/images/bk-1.png') }}" alt="">
                        </div>
                    </div>
                    <div class="col-sm-3 wow fadeInRight animate">
                        <div class="work-fr-you-in">
                            <h4>Upload/Sent Documents</h4>
                            <div class="iner-pr">
                                <p>Documents Required:<br> E-Visa (Option for Hotspot) Scan Copy of passport</p>
                            </div>
                            <img src="{{ asset('/images/bk-2.png') }}" alt="">
                        </div>
                    </div>
                    <div class="col-sm-3 wow fadeInUp animate">
                        <div class="work-fr-you-in">
                            <h4>Delivery of the Parcel</h4>
                            <div class="iner-pr">
                                <p>Your Parcel will be delivered to you given Address (Hotel/AirBnB/ Residence in India) 24 Hrs before your arrival</p>
                            </div>
                            <img src="{{ asset('/images/bk-3.png') }}" alt="">
                        </div>
                    </div>
                    <div class="col-sm-3 wow fadeInUp animate">
                        <div class="work-fr-you-in">
                            <h4>Pickup of the Parcel</h4>
                            <div class="iner-pr">
                                <p>Your Parcel will be Picked up from your Host after your Rental End</p>
                            </div>
                            <img src="{{ asset('/images/bk-4.png') }}" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="video block-element">
    <div class="container">
        <iframe width="560" height="315" src="https://www.youtube.com/embed/Y99VPngO4M0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>
</section>
<section class="deals-acces block-element">
    <div class="container">
        <div class="row">
            <div class="col-sm-4">
                <div class="inner-pro one">
                    <div class="deal-prize">
                        <h6>DEAL<br>
                            <label>STARTS FROM</label><br>
                            $0.000<br>
                            <span>Per Day</span>
                        </h6>
                    </div>
                    <img class="wow fadeInLeft animate" src="{{ asset('/images/pro-1.png') }}" alt="">
                    <a class="bl-btn" href="{{ url('plan-pricing#wifi') }}">Book WI-FI hotspot</a>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="inner-pro two">
                    <img class="wow fadeInUp animate" src="{{ asset('/images/pro-2.png') }}" alt="">
                    <a class="bl-btn" href="{{ url('plan-pricing#phone') }}">Book Phone</a>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="inner-pro three">
                    <div class="deal-prize">
                        <h3>Stay Healthy & <span>Stay <br>Connected</span></h3>
                    </div>
                    <img class="wow fadeInRight animate" src="{{ asset('/images/pro-3.png') }}" alt="">
                    <a class="bl-btn" href="{{ url('plan-pricing') }}">Book Accessory</a>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="culture-section block-element">
    <h2>Know India Before You Arive</h2>
    <h4>Virtual Drive To See India's Rich Culture & Heritage</h4>
    <div class="culture-inner">
        <div class="cult-mor-in">
            <div class="container">
                <div class="home-demo">
                    <div class="owl-carousel">

                        @if ($blogs->count())
                        @foreach($blogs as $i => $blog)
                        <div class="item wow jackInTheBox animate">
                            <div class="client-pro">
                                <img src="{{ URL::to('/').'/images/blogs/'.$blog->image }}" alt="">
                                <a href="{{url('blog/'.$blog->slug)}}"><span>{{$blog->title}}</span></a>
                            </div>
                        </div>
                        @endforeach
                        @endif
                        
                    </div>
                </div>
                <div class="sec-pro-sect home-demo">
                    <div class="owl-carousel">

                        @if ($categories->count())
                        @foreach($categories as $i => $cat)
                        <div class="item wow jackInTheBox animate">
                            <div class="client-pro">
                                <img src="{{ URL::to('/').'/images/categories/'.$cat->image }}" alt="">
                                <a href="{{ url('blogs?category='.$cat->id) }}"> <span>{{$cat->category_name}}</span></a>
                            </div>
                        </div>
                        @endforeach
                        @endif
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('javascript')
<script>
    $(document).ready(function() {

        var owl = $('.owl-carousel');
        owl.owlCarousel({
            margin: 20,
            loop: true,
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 2
                },
                1000: {
                    items: 3
                }
            }
        })
        new WOW().init();
    });
    var owl = $('.home-crousal');
    owl.owlCarousel({
        margin: 20,
        loop: true,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 1
            },
            1000: {
                items: 1
            }
        }
    })
</script>
@endsection