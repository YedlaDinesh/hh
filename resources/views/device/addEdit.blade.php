@extends('layouts.user')
@section('content')
<div class="row">
    <div class="col-md-9 col-center">
        <section class="content-header">
            <h1>Add Device </h1>
            @if(\Session::has('danger'))
            <br />
            <div class="alert alert-danger">
                {{\Session::get('danger')}}
            </div>
            @endif
        </section>
        <section class="content">
            <form role="form" method="post" action="" id="waitform" enctype="multipart/form-data">
                <!-- general form elements disabled -->
                <div class="box">
                    <div class="box-header">
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-12">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>Device Name</label>
                                            <input type="hidden" value="{{csrf_token()}}" name="_token" />
                                            <input type="text" name="device_name" class="form-control" placeholder="Please enter device name" value="{{old('device_name') ?? $device->device_name}}" required />
                                            @error('device_name')
                                            <span class="invalid-feedback" role="alert">
                                                {{ $errors->first('device_name') }}
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label>Device Id</label>
                                            <input type="text" name="device_id" class="form-control" placeholder="Please enter device id" value="{{old('device_id') ?? $device->device_id}}" required />
                                            @error('device_id')
                                            <span class="invalid-feedback" role="alert">
                                                {{ $errors->first('device_id') }}
                                            </span>
                                            @enderror
                                        </div>
                                         <div class="form-group">
                                            <label>App Loaded </label>
                                            <select class="form-control" name="app_loaded" required id="app_loaded">
                                                <option>Select app loaded</option>
                                                @foreach(config('constant.APP_LOADED') as $key => $app_load)
                                                    <option value="{{ $key }}" {{$device->app_loaded == $key ? 'selected' : ''}}>{{ $app_load }}</option>
                                                @endforeach
                                            </select>
                                            @error('app_loaded')
                                            <span class="invalid-feedback" role="alert">
                                                {{ $errors->first('app_loaded') }}
                                            </span>
                                            @enderror
                                        </div>
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-12">
                                       
                                        <div class="form-group">
                                            <label>Device IMEI no.</label>
                                            <input type="hidden" value="{{csrf_token()}}" name="_token" />
                                            <input type="number" name="device_imei" class="form-control" placeholder="Enter device IMEI no" value="{{old('device_imei') ?? $device->device_imei}}" required />
                                            @error('device_imei')
                                            <span class="invalid-feedback" role="alert">
                                                {{ $errors->first('device_imei') }}
                                            </span>
                                            @enderror
                                        </div>
                                         <div class="form-group">
                                            <label>Device Type</label>
                                            <select class="form-control" name="device_type" required id="device_type">
                                                <option>Select device type</option>
                                                @foreach(config('constant.DEVICE_TYPE') as $key => $device_type)
                                                    <option value="{{ $key }}" {{$device->device_type == $key ? 'selected' : ''}}>{{ $device_type }}</option>
                                                @endforeach
                                            </select>
                                            @error('device_type')
                                            <span class="invalid-feedback" role="alert">
                                                {{ $errors->first('device_type') }}
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <a href="{{ url('admin/device/list') }}" class="btn btn-danger">&larr; Cancel</a> <button type="submit" class="btn btn-primary pull-right">Submit</button>
                    </div>
                </div>
            </form>
        </section>
    </div>
</div>
@endsection
