@extends('layouts.user')

@section('content')
<section class="content-header">
    <h1>Manage Devices</h1>
    @if(\Session::has('success'))
    <br />
    <div class="alert alert-success">
        {{\Session::get('success')}}
    </div>
    @endif
    @if(\Session::has('danger'))
    <br />
    <div class="alert alert-danger">
        {{\Session::get('danger')}}
    </div>
    @endif
</section>
<section class="content">
    <form role="form" method="post" action="{{url('admin/device/listaction')}}" id="list_client_form">
        <input type="hidden" value="{{csrf_token()}}" name="_token" />
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Devices List</h3>
                <a href="{{ url('admin/device/addedit/0') }}" class="btn btn-warning pull-right">Add New</a>
            </div><!-- /.box-header -->
            <div class="box-body">
                <table id="tablelist" class="table table-bordered table-striped" width="100%">
                    <thead>
                        <tr>
                            <th class="text-center" width="5%"><input type="checkbox" name="chktgl" title="Select/Unselect All" class="seldel"></th>
                            <th>S.no</th>
                            <th>Device Name</th>
                            <th>Device Id</th>
                            <th>Device IMEI No</th>
                            <th>App Loaded</th>
                            <th>Device Type</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if ($devices->count())
                        @foreach($devices as $i => $device)
                        <tr>
                            <td class="text-center"><input type="checkbox" name="del[]" value="{{$device->id}}" class="delids"></td>
                            <td>{{$i+1}}</td>
                            <td>{{$device->device_name}}</td>
                            <td>{{$device->device_id}}</td>
                            <td>{{$device->device_imei}}</td>
                            <td>
                                {{ ($device->app_loaded == 1) ? "Provisioned": ( ($device->app_loaded == 2) ? "Active": (($device->app_loaded == 3) ? "Lost": "Inactive")) }}
                            </td>
                            <td>
                                {{ ($device->device_type == 1) ? "Mobile": 'Wifi Hotspot' }}
                            </td>
                            <td>
                                <a href="{{ url('admin/device/addedit/'.$device->id) }}" class="btn btn-primary">Edit</a>&nbsp&nbsp
                            </td>
                        </tr>
                        @endforeach
                        @else
                        <tr>
                            <td class="text-center" colspan="7">-No devices found-</td>
                        </tr>
                        @endif
                    </tbody>
                    
                </table>

            </div><!-- /.box-body -->
            @if ($devices->count())
            <div class="box-footer">
                <button type="submit" class="btn btn-danger">Delete</button>
            </div>
            @endif
    </form>
</section>
@endsection

@section('javascript')
@if ($devices->count())
<script type="text/javascript">

    $(document).ready(function () {
        $("#tablelist").dataTable();

        $('.alert').fadeOut(4000);

        function validatechk()
        {
            
            var status = false;

            if ($('.delids:checkbox:checked').length == '0') {
                $.alert({
                    title: 'Alert!',
                    content: 'Please select atleast one!',
                });
                status = false;
                return status;
            }

            if (confirm("Are you sure to DELETE?")) {
                status = true;
                return status;
            }
            return status;
        }


        $('#list_client_form').submit(validatechk);


//        $('#sd').submit(function (e) {
//            e.preventDefault();
//
//
//
//            $.confirm({
//                title: 'Confirm!',
//                content: 'Simple confirm!',
//                buttons: {
//                    confirm: function () {
//                        $.alert('Confirmed!');
//                    },
//                    cancel: function () {
//                        $.alert('Canceled!');
//                    },
//
//                }
//            });
//        });


    });
</script>
@endif
@endsection