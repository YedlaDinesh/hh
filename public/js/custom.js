$(document).ready(function(){

   
$(".deviceDate").each(function(){
    $("#bookNowForm").validate({
        rules: {
            email: {
                required: true,
                email: true
            },
            start_date: {
                required: true
            },
            end_date: {
                required: true
            },
        },
        highlight: function(element) {
            $(element).closest('.control-group').removeClass('success').addClass('error');
        },
        success: function(element) {
            element.closest('.control-group').removeClass('error').addClass('success');
        }
    });
});
 });