<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Faq extends Model
{
    //
     public function saveFaq($data) {

        $id = $data['id'];
        $question = $data['question'];
        $answer = $data['answer'];
        $category= $data['category'];

        // die($id);
        if ($data['id'] > 0) {
            $faq = $this::where([
                        ['id', '=', $data['id']]
                    ])->first();
            if (!$faq) {
                $faq = $this;
            }
        } else {
            $faq = $this;
        }
        
        $faq->question = $data['question'];
        $faq->answer = $data['answer'];
        $faq->category = $data['category'];
        
        try {
            $faq->save();
            $jdata = json_encode(array('status' => 'success'));
            return $jdata;
        } catch (Exception $e) {
            //dd($e->getMessage());
            $jdata = json_encode(array('status' => 'failed'));
            return $jdata;
        }
	}

	public function deleteFaq($delids) {

        foreach ($delids as $id) {
            $faq = $this->find($id);
            $faq = $this::where([
                        ['id', '=', $id]
                    ])->delete();
        }
        return 1;
    }
    
    public function scopeIsActive($query)
    {
        return $query->where('status', 1);
    }

}
