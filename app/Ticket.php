<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Ticket extends Model
{
    protected $fillable = ['orderId', 'email', 'name', 'query'];

    public function saveTicket($data) {
        
        $validTicket =  \DB::table('orders') ->where([
                    ['id', '=', $data['orderId']],
                    ['customerEmail', '=', $data['email']]
                ])->first();
        if ($validTicket) {
           
            try {
                $ticket = $this::create($data);
                $data['status']     = "success";
                $data['result']     = "Your ticket submited successfully ";
            } catch (Exception $e) {
                $data['status']     = "danger";
                $data['result']     = $e->getMessage();
            }
        } else {
            $data['status']     = "danger";
            $data['result']     = "Kindly check your email or order number";
        }
        return json_encode($data);
	}
}
