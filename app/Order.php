<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Booking;

class Order extends Model
{
    //
    public function bookings()
    {
        return $this->hasMany('App\Booking', 'orderUniqueId', "bookForeignId");
    }
    

}
