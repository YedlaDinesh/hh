<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categorie extends Model
{
    //
     public function saveCategorie($data) {

        $id = $data['id'];
        //die($id);
        if ($data['id'] > 0) {
            $categorie = $this::where([
                        ['id', '=', $data['id']]
                    ])->first();
            if (!$categorie) {
                $categorie = $this;
            }
        } else {
            $categorie = $this;
        }
        
        $categorie->category_name = $data['category_name'];
        $categorie->image = (!empty($data['image'])) ? $data['image'] : $categorie->image;
        
        try {
            $categorie->save();
            $jdata = json_encode(array('status' => 'success'));
            return $jdata;
        } catch (Exception $e) {
            //dd($e->getMessage());
            $jdata = json_encode(array('status' => 'failed'));
            return $jdata;
        }
	}

	public function deleteCategorie($delids) {

        foreach ($delids as $id) {
            $categorie = $this->find($id);
            $categorie = $this::where([
                        ['id', '=', $id]
                    ])->delete();
        }
        return 1;
    }
    
    public function scopeIsActive($query)
    {
        return $query->where('status', 1);
    }

}
