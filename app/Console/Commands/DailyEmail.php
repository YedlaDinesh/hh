<?php

namespace App\Console\Commands;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMail;
use App\User;
use App\Order;
use Carbon\Carbon;

class DailyEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'usermail:daily';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Respectively send an exclusive information to everyone daily via email.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }  

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

               date_default_timezone_set('America/Los_Angeles');
                $allorder = Order::all();

					foreach ($allorder as $order) {
                        
                            if($order->step === '1'){
                                $order_page_url = url("/procees-page/{$order->id}");
                               Mail::raw("Please click on the url = <a href=".$order_page_url.">Click Here</a>", function ($mail) use ($order) {
                                    $mail->from('kmajay978@gmail.com');
                                    $mail->to($order->customerEmail)
                                        ->subject('Plesae Complete your Details! And upload your details');
                                });
                                    
                             
                            }
                            elseif($order->step === '2')
                            {
                                
                                $order_page_url = url("/procees-page/{$order->id}");
                                Mail::raw("Please click on the url = <a href=".$order_page_url.">Click Here</a>", function ($mail) use ($order) {
                                    $mail->from('kmajay978@gmail.com');
                                    $mail->to($order->customerEmail)
                                        ->subject('Plesae Complete your Details! And upload your details');
                                });  
                                    
                            }
                        
                    }
         
        $this->info('Successfully sent daily Email to the Users.');
    }
}