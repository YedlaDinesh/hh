<?php

namespace App\Http\Controllers;

use App\Plan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PlanController extends Controller
{
    // public function __construct() {
    //     $this->middleware('auth');
    // }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function listMobile()
    {
        $plans = DB::table('mobileplans')->get();
        return view('plans.listMobile', compact('plans'));
        
    }

    public function listWifi()
    {
        $plans = DB::table('wifiplans')->get();
        return view('plans.listWifi', compact('plans'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeMobile(Request $request)
    {
        $id = $request['id'];
        
        $update = \DB::table('mobileplans') ->where('id', $id) ->limit(1) 
        ->update( [ 'name' => $request->input('name'), 'price' => $request->input('price'), 'calling' => $request->input('calling'), 'data' => $request->input('data')]);

        return redirect('admin/planMobile/list')->with('success', 'Mobile Plan Updated Successfully');
    }

    public function storeWifi(Request $request)
    {
        $id = $request['id'];
        $update = \DB::table('wifiplans') ->where('id', $id) ->limit(1) 
        ->update( [ 'name' => $request->input('name'), 'price' => $request->input('price'), 'data' => $request->input('data')]);

        return redirect('admin/planWifi/list')->with('success', 'Wifi Plan Updated Successfully');
    }
  
    public function addeditMobile($id) {

        $plan = DB::table('mobileplans')->where('id', '=', $id)
                ->first();
        if (!$plan) {
            $plan = new Plan;
        }
        return view('plans.addEditMobile', compact('plan'));
    }

    public function addeditWifi($id) {

        $plan = DB::table('wifiplans')->where('id', '=', $id)
                ->first();
        if (!$plan) {
            $plan = new Plan;
        }
        return view('plans.addEditWifi', compact('plan'));
    }

    public function listaction(Request $request) {
        
        $delids = $request['del'];
        //echo '<pre>';print_r($delids);exit;
        if (!empty($delids)) {
            
            $plan = new Plan();
            $plan->deletePlan($delids);
            
            return redirect('admin/plan/list')->with('success', 'Client Deleted!');
        } else {
            return redirect('admin/plan/list')->with('danger', 'No record selected!');
        }
    }

    public function getPlans(){
        $mobilePlans = DB::table('mobileplans')->get();
        $container = ['one', 'two', 'three'];
        $wifiPlans = DB::table('wifiplans')->get();
        return view('home.plan-pricing', compact('mobilePlans','container','wifiPlans'));
    }
}
