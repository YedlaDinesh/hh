<?php

namespace App\Http\Controllers;

use App\Ticket;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Order;

class TicketController extends Controller
{

    // public function __construct() {
    //     $this->middleware('auth');
    // }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $tickets = Ticket::all();
        return view('tickets.list', compact('tickets'));
    }

    public function viewPage()
    {
        //
        $rets['status'] = "";
        return view('home.ticket-page', compact('rets'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $ticket = new ticket();  
      $data = $this->validate($request, [
        'orderId' => 'required',
        'email' => 'required',
        'name' => 'required',
        'query' => 'required'
    ]);

        $ret = $ticket->saveTicket($data);
        $rets = json_decode($ret, true);

        return view('home.ticket-page', compact('rets'));
        //return  redirect()->route('home.ticket-page');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function show(Ticket $ticket)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function edit(Ticket $ticket)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $id = $request['id'];
         $status = $request->input('status');

         $update = \DB::table('tickets') ->where('id', $id) ->limit(1) 
        ->update( [ 'status' => $status]);
       
        return redirect('admin/ticket/list')->with('success', 'Ticket Status Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ticket $ticket)
    {
        //
    }

    public function find($id) {

        $ticket = Ticket::find($id);
        return view('tickets.view', compact('ticket'));
    }
    
}
