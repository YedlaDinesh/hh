<?php

namespace App\Http\Controllers;

use App\Accessorie;
use Illuminate\Http\Request;

class AccessorieController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $accessories = Accessorie::get();
        
        return view('accessorie.list', compact('accessories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        //
        $accessorie = new Accessorie();
        $id = $request['id'];
        if ($id == '0') {
            $data = $this->validate($request, [
                'accessorie_name' => 'required',
                'description' => 'required',
                'price' => 'required'
            ]);
        } else {
            $data = $this->validate($request, [
                'accessorie_name' => 'required',
                'description' => 'required',
                'price' => 'required'
            ]);
        }

        $data['id'] = $request['id'];

        $ret = $accessorie->saveAccessorie($data);
        $rets = json_decode($ret, true);
        //echo '<pre>';print_r($rets);exit;

        if ($rets['status'] == 'failed') {
            die('failed');
        }

        return redirect('admin/accessorie/list')->with('success', 'Accessorie Saved!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\devices  $devices
     * @return \Illuminate\Http\Response
     */
    public function show(devices $devices)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\devices  $devices
     * @return \Illuminate\Http\Response
     */
    public function edit(devices $devices)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\devices  $devices
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, devices $devices)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\devices  $devices
     * @return \Illuminate\Http\Response
     */
    public function destroy(devices $devices)
    {
        //
    }

    public function addedit($id) {

        $accessorie = accessorie::where('id', '=', $id)
                ->first();
        if (!$accessorie) {
            $accessorie = new accessorie;
        }
        return view('accessorie.addEdit', compact('accessorie'));
    }

    public function listaction(Request $request) {
        
        $delids = $request['del'];
        //echo '<pre>';print_r($delids);exit;
        if (!empty($delids)) {
            
            $accessorie = new accessorie();
            $accessorie->deleteAccessorie($delids);
            
            return redirect('admin/accessorie/list')->with('success', 'Accessorie Deleted!');
        } else {
            return redirect('admin/accessorie/list')->with('danger', 'No record selected!');
        }
    }
}
