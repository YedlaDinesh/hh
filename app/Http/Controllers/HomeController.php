<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
        //return redirect(route('admin/home'));
    }

    //Check login 
    public function checkLogin(){

        if (auth()->user()) 
        {
            return redirect(route('home'));
        }
        else
        {
            return view('auth.login');
        }
    }

}
