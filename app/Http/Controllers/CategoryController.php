<?php

namespace App\Http\Controllers;

use App\Categorie;
use Illuminate\Http\Request;
use Image;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Collection;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $categories = Categorie::get();
        
        return view('categorie.list', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        //
        $categorie = new Categorie();
        $id = $request['id'];
        if ($id == '0') {
            $data = $this->validate($request, [
                'category_name' => 'required|unique:categories,category_name',
                'image' => 'required|mimes:jpeg,png,jpg',
            ]);
        } else {
            $data = $this->validate($request, [
                'category_name' => 'required|unique:categories,category_name,'.$id .'id',
                'image' => 'mimes:jpeg,png,jpg',
            ]);
        }

        $data['id'] = $request['id'];

        $img= $request->file('image');
        if(!empty($img)){
           
            $blogImage = Image::make($img);
            $originalPath = public_path().'/images/categories/';
            $blogImage->save($originalPath.time().$img->getClientOriginalName());
            $blogImage->resize(310,170);

            $data['image'] = time().$img->getClientOriginalName();
        }
       



        $ret = $categorie->saveCategorie($data);
        $rets = json_decode($ret, true);
        //echo '<pre>';print_r($rets);exit;

        if ($rets['status'] == 'failed') {
            die('failed');
        }

        return redirect('admin/category/list')->with('success', 'Categorie Saved!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\devices  $devices
     * @return \Illuminate\Http\Response
     */
    public function show(devices $devices)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\devices  $devices
     * @return \Illuminate\Http\Response
     */
    public function edit(devices $devices)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\devices  $devices
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, devices $devices)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\devices  $devices
     * @return \Illuminate\Http\Response
     */
    public function destroy(devices $devices)
    {
        //
    }

    public function addedit($id) {

        $categorie = Categorie::where('id', '=', $id)
                ->first();
        if (!$categorie) {
            $categorie = new Categorie;
        }
        return view('categorie.addEdit', compact('categorie'));
    }

    public function listaction(Request $request) {
        
        $delids = $request['del'];
        //echo '<pre>';print_r($delids);exit;
        if (!empty($delids)) {
            
            $categorie = new Categorie();
            $categorie->deleteCategorie($delids);
            
            return redirect('admin/category/list')->with('success', 'Categorie Deleted!');
        } else {
            return redirect('admin/category/list')->with('danger', 'No record selected!');
        }
    }
}
