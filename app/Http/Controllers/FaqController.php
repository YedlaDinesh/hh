<?php

namespace App\Http\Controllers;

use App\Faq;
use Illuminate\Http\Request;

class FaqController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $Faqs = Faq::get();
        // dd($Faqs);
        return view('faq.list', compact('Faqs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        //
        $Faq = new Faq();
        $id = $request['id'];

    
        $data = [];
        $data['id'] = $request['id'];
        $data['question'] = $request['question'];
        $data['answer'] = $request['answer'];
        $data['category'] = $request['category'];

        $ret = $Faq->saveFaq($data);
        $rets = json_decode($ret, true);
        //echo '<pre>';print_r($rets);exit;

        if ($rets['status'] == 'failed') {
            die('failed');
        }

        return redirect('admin/faq/list')->with('success', 'Faq Saved!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\devices  $devices
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        // Displaying on faq page
        $general = Faq::get()->where('category', '1');
        $pocket = Faq::get()->where('category', '2');
        $delivery = Faq::get()->where('category', '3');
        $network = Faq::get()->where('category', '4');
        $technical = Faq::get()->where('category', '5');
        $payment = Faq::get()->where('category', '5');
        //  dd($faqlist);
        return view('home.faq', compact('general','pocket','delivery','network','technical','payment'));
       

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\devices  $devices
     * @return \Illuminate\Http\Response
     */
    public function edit(devices $devices)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\devices  $devices
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, devices $devices)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\devices  $devices
     * @return \Illuminate\Http\Response
     */
    public function destroy(devices $devices)
    {
        //
    }

    public function addedit($id) {

        $faq = Faq::where('id', '=', $id)
                ->first();
        if (!$faq) {
            $faq = new Faq;
        }
        return view('faq.addEdit', compact('faq'));
    }

    public function listaction(Request $request) {
        
        $delids = $request['del'];
        //echo '<pre>';print_r($delids);exit;
        if (!empty($delids)) {
            
            $faq = new Faq();
            $faq->deleteFaq($delids);
            
            return redirect('admin/faq/list')->with('success', 'faq Deleted!');
        } else {
            return redirect('admin/faq/list')->with('danger', 'No record selected!');
        }
    }
}
