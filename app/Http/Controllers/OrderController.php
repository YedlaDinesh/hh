<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Order;

class OrderController extends Controller
{

    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $orders = Order::all();
        return view('orders.list', compact('orders'));
    }

    public function show(Ticket $ticket)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function edit(Ticket $ticket)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $id = $request['id'];
         $status = $request->input('status');

         $update = \DB::table('orders') ->where('bookForeignId', $id) ->limit(1) 
        ->update( [ 'status' => $status]);
       
        return redirect('admin/order/list')->with('success', 'Order Status Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ticket $ticket)
    {
        //
    }

    public function find($orderId) {

        $orders = Order :: with('bookings')->where('bookForeignId', $orderId)
            ->first();
        return view('orders.view', compact('orders'));
    }
    
    public function getCustomer(){
        $customers = Order::all();
        return view('customer.list', compact('customers'));
    }


    public function listLeads()
    {
        $leads = DB::table('bookings')
        ->where('paymentStatus', null)
        ->get()->unique('email');
        
        return view('leads.listLeads', compact('leads'));
        
    }

    public function listPayments()
    {

        $orders = Order::all();

        return view('payments.listPayments', compact('orders'));
        
    }
    
}