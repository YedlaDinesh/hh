<?php

namespace App\Http\Controllers;

use App\device;
use Illuminate\Http\Request;

class DeviceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $devices = device::get();
        
        return view('device.list', compact('devices'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $device = new device();
        $id = $request['id'];
        if ($id == '0') {
            $data = $this->validate($request, [
                'device_name' => 'required',
                'device_id' => 'required|unique:devices',
                'device_imei' => 'required',
                'app_loaded' => 'required',
                'device_type' => 'required'
            ]);
        } else {
            $data = $this->validate($request, [
                'device_name' => 'required',
                'device_id' => 'required|unique:devices',
                'device_imei' => 'required',
                'app_loaded' => 'required',
                'device_type' => 'required'
            ]);
        }

        $data['id'] = $request['id'];

        $ret = $device->saveDevice($data);
        $rets = json_decode($ret, true);
        //echo '<pre>';print_r($rets);exit;

        if ($rets['status'] == 'failed') {
            die('failed');
        }

        return redirect('/admin/device/list')->with('success', 'Device Saved!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\devices  $devices
     * @return \Illuminate\Http\Response
     */
    public function show(devices $devices)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\devices  $devices
     * @return \Illuminate\Http\Response
     */
    public function edit(devices $devices)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\devices  $devices
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, devices $devices)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\devices  $devices
     * @return \Illuminate\Http\Response
     */
    public function destroy(devices $devices)
    {
        //
    }

    public function addedit($id) {

        $device = device::where('id', '=', $id)
                ->first();
        if (!$device) {
            $device = new device;
        }
        return view('device.addEdit', compact('device'));
    }

    public function listaction(Request $request) {
        
        $delids = $request['del'];
        //echo '<pre>';print_r($delids);exit;
        if (!empty($delids)) {
            
            $device = new device();
            $device->deleteDevice($delids);
            
            return redirect('/device/list')->with('success', 'Device Deleted!');
        } else {
            return redirect('/device/list')->with('danger', 'No record selected!');
        }
    }
}
