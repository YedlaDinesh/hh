<?php

namespace App\Http\Controllers;

use App\Blog;
use App\Categorie;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Cviebrock\EloquentSluggable\Services\SlugService;
use Image;
use Illuminate\Support\Facades\Storage;

class BlogsController extends Controller
{

    public function home()
    {
        $blogs = Blog::with('category')->where('is_published', 1)->get();
        $categories = Categorie::get();
        return view('welcome', compact('blogs', 'categories'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blogs = Blog::with('category')->get();
        return view('blog.list', compact('blogs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $blog = new Blog();
        $id = $request['id'];
        if ($id == '0') {
            $data = $this->validate($request, [
                'title' => 'required',
                'description' => 'required',
                'image' => 'required|mimes:jpeg,png,jpg',
                'cat_id' => 'required',
                'slug' => 'required|unique:blogs,slug',
                'is_published' => 'required'
            ]);
        } else {
            $data = $this->validate($request, [
                'title' => 'required',
                'description' => 'required',
                'image' => 'mimes:jpeg,png,jpg',
                'cat_id' => 'required',
                'slug' => 'required|unique:blogs,slug,' . $id . 'id',
                'is_published' => 'required'
            ]);
        }

        $data['id'] = $request['id'];

        $img = $request->file('image');
        if (!empty($img)) {

            $blogImage = Image::make($img);
            $thumbnailPath = public_path() . '/images/blogs/thumbnail/';
            $originalPath = public_path() . '/images/blogs/';
            $blogImage->save($originalPath . time() . $img->getClientOriginalName());
            $blogImage->resize(310, 170);
            $blogImage->save($thumbnailPath . time() . $img->getClientOriginalName());

            $data['image'] = time() . $img->getClientOriginalName();
        }
        $ret = $blog->saveBlog($data);
        $rets = json_decode($ret, true);

        if ($rets['status'] == 'failed') {
            die('failed');
        }

        return redirect('admin/blog/list')->with('success', 'Blog Saved!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\devices  $devices
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $blog_post = Blog::where('slug', $slug)
            ->where('is_published', '1')
            ->first();
        $categories = Categorie::where('status', '1')->get();
        $recent_post = Blog::latest()->take(5)->where('is_published', 1)->get();

        return view('home.blog_single', compact('blog_post', 'recent_post', 'categories'));
    }

    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\devices  $devices
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, devices $devices)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\devices  $devices
     * @return \Illuminate\Http\Response
     */
    public function destroy(devices $devices)
    {
        //
    }

    public function addedit($id)
    {
        $categories = Categorie::get()->pluck('category_name', 'id');
        $blog = Blog::where('id', '=', $id)
            ->first();
        if (!$blog) {
            $blog = new Blog;
        }

        return view('blog.addEdit', compact('blog', 'categories'));
    }

    public function listaction(Request $request)
    {
        $delids = $request['del'];
        if (!empty($delids)) {
            $blog = new Blog();
            $blog->deleteBlog($delids);

            return redirect('admin/blog/list')->with('success', 'Blog Deleted!');
        } else {
            return redirect('admin/blog/list')->with('danger', 'No record selected!');
        }
    }

    public function check_slug(Request $request)
    {
        $slug = SlugService::createSlug(Blog::class, 'slug', $request->title);
        return response()->json(['slug' => $slug]);
    }


    public function showblog(Request $request)
    {
        $categoryId = $request->category;
        if($categoryId != "" && is_numeric($categoryId)){
            $allblogs = Blog::where('cat_id', $categoryId)->where('is_published', 1)->paginate(5);
        }else{
            $allblogs = Blog::where('is_published', 1)->paginate(5);
        }
        $recent_post = Blog::latest()->take(5)->where('is_published',1)->get();
        $categories = Categorie::get();
        
        return view('home.blog', compact('allblogs', 'recent_post', 'categories'));
    }
}
