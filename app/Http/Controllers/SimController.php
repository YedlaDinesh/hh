<?php

namespace App\Http\Controllers;

use App\sim;
use Illuminate\Http\Request;

class SimController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sims = sim::get();
        return view('sim.list', compact('sims'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $sim = new sim();
        $id = $request['id'];
        if ($id == '0') {
            $data = $this->validate($request, [
                'sim_iccid' => 'required|numeric',
                'sim_id' => 'required|unique:sims',
                'sim_no' => 'required|digits:10',
                'sim_status' => 'required',
                'sim_type' => 'required',
                'start_date' => 'date_format:d/m/Y',
                'end_date' => 'date_format:d/m/Y',
            ]);
        } else {
            $data = $this->validate($request, [
                'sim_iccid' => 'required|numeric',
                'sim_id' => 'required|unique:sims',
                'sim_no' => 'required|digits:10',
                'sim_status' => 'required',
                'sim_type' => 'required',
                'start_date' => 'date_format:d/m/Y',
                'end_date' => 'date_format:d/m/Y',
            ]);
        }

        $data['id'] = $request['id'];

        $ret = $sim->saveSim($data);
        $rets = json_decode($ret, true);
        //echo '<pre>';print_r($rets);exit;

        if ($rets['status'] == 'failed') {
            die('failed');
        }

        return redirect('/admin/sim/list')->with('success', 'Sim Saved!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\sim  $sim
     * @return \Illuminate\Http\Response
     */
    public function show(sim $sim)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\sim  $sim
     * @return \Illuminate\Http\Response
     */
    public function edit(sim $sim)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\sim  $sim
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, sim $sim)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\sim  $sim
     * @return \Illuminate\Http\Response
     */
    public function destroy(sim $sim)
    {
        //
    }

    public function addedit($id) {

        $sim = sim::where('id', '=', $id)
                ->first();
        if (!$sim) {
            $sim = new sim;
        }
        return view('sim.addEdit', compact('sim'));
    }

    public function listaction(Request $request) {
        
        $delids = $request['del'];
        if (!empty($delids)) {
            $sim = new sim();
            $sim->deleteSim($delids);
            
            return redirect('/sim/list')->with('success', 'Sim Deleted!');
        } else {
            return redirect('/sim/list')->with('danger', 'No record selected!');
        }
    }

}
