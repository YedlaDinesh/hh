<?php

namespace App\Http\Controllers;

use App\Plan;
use App\Booking;
use App\Order;
use Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Mail;

class BooknowController extends Controller
{

	public function getDevice(Request $request){
		$deviceid = $request->deviceid;
		$planid = $request->planid;
		if($deviceid != null && $planid != null){
			$plans = $this->getDevicePlan($deviceid);
		}else{
			return redirect()->to('plan-pricing');
		}
		$accessories = DB::table('accessories')->get();
		return view('home.book-now', compact('accessories','plans'));
	}

	 /*Function for upload media*/
  private function upload_media($media, $upload_media_path)
  {
	$current_timestamp = Carbon::now()->timestamp; // Produces something like 1552296328
	$media_name = $current_timestamp . "_" . $media->getClientOriginalName();
	for($i = 2;; $i ++) 
	{
	  if(file_exists(base_path() . $upload_media_path . $media_name)) 
	  {
		  $media_name = $i . "_" . $media_name;
	  } 
	  else
	  {
		  break;
	  }
	}
	$media->move(base_path().'/'.$upload_media_path, $media_name);
	return $media_name;
  }

	public function getDevicePlan($deviceId){
		switch ($deviceId) {
			case 1:
			   return DB::table('mobileplans')->get();
			break;
			case 2:
				return DB::table('wifiplans')->get();            
			break;
		}
	}

	public function getPlanPrice($planId){
	   if($planId != ""){
			return DB::table('mobileplans')->where("id", "=", $planId)->get();
		}
	}

	

	public function bookDevice(Request $request){
		
		$data = $request->input('deviceFormData');
		
		// echo '<pre>';
		// print_r($data);
		// die();
		$uniqueId = floor(time() - 999999999);
		//$uniqueId = strtoupper(substr(uniqid(sha1(time())),0,4));
		$proceed = false;
		$data = ($data['items']);
		$msg = array();
		$orderData = array();
		$email = $request->input('deviceFormData')['email'];
		$orderData['email'] = $email;
		$orderData['total'] = $request->input('deviceFormData')['toatlAmt'];
		$orderData['uniqueId'] = $uniqueId;
			foreach($data as $d){
				$booking = new Booking;
				$booking->email = $email;
				$booking->deviceId = $d['deviceId'];
				$booking->planId = $d['planId'];
				$booking->days = $d['days'];
				$booking->devicePrice = $d['price'];
				$booking->deviceQty = $d['qnt'];
				if(array_key_exists('accessorie',$d)){
				$accessories = $d['accessorie'];
	
				if(!empty($accessories['accessorieId'])){
					$booking->accessorieId = $accessories['accessorieId'];
					$booking->accessoriePrice = $accessories['accessoriePrc'];
					$booking->accessorieQty = $accessories['accessorieQnt'];
				}
			}
			$booking->orderUniqueId = $uniqueId;
				if($booking->save()){
					$proceed = true;
				}
			}
		if($proceed){
			$this->saveOrder($orderData);
		}else{
			$msg['status'] = 201;
			$msg['result'] = "Lead not genrated succesfully";
			echo json_encode($msg);
		}
	}

	public function getStatusOK($message,$response) {
		$response = ["status_code" => 200, "message" => $message, "error" => false, "error_message" => "", "data" => $response];
		return response()->json($response, 200, $headers = [], $options = JSON_PRETTY_PRINT);
	  }

	public function saveOrder($orderData){
		//post saving of paypal
		$order = new Order;
		$order->customerEmail = $orderData['email'];
		$order->totalAmount = $orderData['total'];
		$order->bookForeignId = $orderData['uniqueId'];
		// $order->paypalId = $uniqueId;
		// $order->paypalData = $uniqueId;
		// $order->transactionId = $uniqueId;
		$order->step = 1;
		$data = [];
		if($order->save()){
			$data['status'] = 200;
			$data["orderId"] = $order->id;
			$order = Order::findOrFail($order->id);
			$html_template = 'Order Id = '.$order->id.' Order Email = '.$order->customerEmail.' Total Amount = '.$order->totalAmount;
			Mail::raw($html_template, function ($mail) use ($order) {
				$mail->from('codexade978@gmail.com');
				$mail->to($order->customerEmail)
					->subject('Your Order successfull');
			});

			
			
		}
		else{
			$data['status'] = 201;
			$data["result"] = "Order not genrated succesfully";
		}
		echo json_encode($data);
	}

	public function processPage(Request $request)
	{
		$id = $request['id'];
		$order = Order::find($id);
		//echo '<pre>';
		//print_r($order->step);
		if($order->step === '1'){
			return view('home.procees-page');
		}elseif($order->step === '2')
		{
			return view('home.deliver-page');
		}
		elseif($order->step === '3')
		{
			return view('home.orderSuccess');
		}
		//die();
		return view('home.procees-page');
		
	 }
		
	public function processPageUpload(Request $request ){
			$current_timestamp = Carbon::now()->timestamp;
				$order_id = $request['id'];
				$order = Order::find($order_id);
			  
				if($order->step === '1'){
		   
					$doc1 = $request->file("doc1"); 
					$doc2 = $request->file("doc2"); 
					$doc3 = $request->file("doc3"); 
					if($doc1 != null)
					{
						$doc1_image_name=$this->upload_media($doc1,'/public/images/');
					}
					if($doc2 != null)
					{
						$doc2_image_name=$this->upload_media($doc2,'/public/images/');
					}
					if($doc3 != null)
					{
						$doc3_image_name=$this->upload_media($doc3,'/public/images/');
					}
				
					$order->step = 2;
					$order->fullName = $request->full_name;
					$order->fathersName = $request->father;
					$order->residientAddress = $request->address;
					$order->street = $request->street;
					$order->postalCode = $request->pincode;
					$order->city = $request->city;
					$order->country = $request->country;
					$order->phoneNumber = $request->phone;

					$order->doc1 = $doc1_image_name ? $doc1_image_name : "";
					$order->doc2 = $doc2_image_name ? $doc2_image_name : "" ;
					$order->doc3 = $doc3_image_name ? $doc3_image_name : "";

					$order->created_at = $current_timestamp;
					$order->updated_at = $current_timestamp;
					if($order->save())
					{
						$order = Order::findOrFail($order->id);

			
							$html_template = 'Your Name = '.$order->fullName.' Father Name = '.$order->fathersName.' Address = '.$order->residientAddress. 'Street Address = '.$order->street.' Pincode = '.$order->postalCode.' Phone Number = '.$order->phoneNumber;
						
							Mail::raw($html_template, function ($mail) use ($order) {
								$mail->from('codexade978@gmail.com');
								$mail->to($order->customerEmail)
									->subject('Your Order successfull');
							});
					}
					return redirect("procees-page/$order_id")->with('message','Order Updated successfully');
			//  dd($request);
					
				}elseif($order->step === '2')
				{
					$current_timestamp = Carbon::now()->timestamp;
					$order_id = $request['id'];
					$order = Order::find($order_id);
				  
						$order->step = 3;
						$order->deliveryType = $request->type_address;
						$order->deliveryAddress = $request->full_address;
						$order->deliveryPostalCode = $request->pincode;
						$order->deliveryCity = $request->city;
						$order->deliveryState = $request->state;
	 
						$order->pickupType = $request->type_address;
						$order->pickupAddress = $request->full_address;
						$order->pickupPostalCode = $request->pincode;
	 
						$order->pickupCity = $request->city;
						$order->pickupState = $request->state;
						$order->created_at = $current_timestamp;
						$order->updated_at = $current_timestamp;
						$order->save();
						return redirect("procees-page/$order_id")->with('message','Delivery address Updated successfully');
				 //  dd($request);
				   
				}
				elseif($order->step === '3')
				{
					return redirect("procees-page/$order_id")->with('message','Order Successfully');
				}
				  
		}


			 public function paymentSuccess(Request $request)
			 {
				$id = $request['id'];
				$order = Order::find($id);
				if($order != null){
		return view('home.paymentSuccess',compact('id'));
				}
				die('asd');
				 }

				 public function login(){
					return view('home.login');
					 }

					 public function loginCheck(Request $request){
						
						$order_id = $request->orderid;
						$Email = $request->email;
						$order = Order::find($order_id);
						if($order != null && $order->customerEmail === $Email)
						{
							return redirect("procees-page/$order_id")->with('message','Order Successfully');
						}
						else
						{
							Session::flash('alert-success', 'Please Enter Valid Order id and Email');
							return redirect('login');
						}
					}
}