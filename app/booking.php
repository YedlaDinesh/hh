<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Order;

class Booking extends Model
{
    //
    
    public function order()
    {
        return $this->hasOne('App\Order',"bookForeignId", 'orderUniqueId');
    }

}