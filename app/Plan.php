<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{
    //
    public function savePlan($data) {

        $id = $data['id'];
        //die($id);
        if ($data['id'] > 0) {
            $plan = $this::where([
                        ['id', '=', $data['id']]
                    ])->first();
            if (!$plan) {
                $plan = $this;
            }
        } else {
            $plan = $this;
        }
        
        
        $plan->name = $data['name'];
        $plan->description = $data['description'];
        $plan->image = $data['image'];
        $plan->price = $data['price'];
        try {
            $plan->save();
            $jdata = json_encode(array('status' => 'success'));
            return $jdata;
        } catch (Exception $e) {
            //dd($e->getMessage());
            $jdata = json_encode(array('status' => 'failed'));
            return $jdata;
        }
    }
    public function deletePlan($delids) {

        foreach ($delids as $id) {
            $plan = $this->find($id);
            $plan = $this::where([
                        ['id', '=', $id]
                    ])->delete();
        }
        return 1;
    }
}
