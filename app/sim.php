<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class sim extends Model
{
    //
    public function saveSim($data) {

        $id = $data['id'];
        //die($id);
        if ($data['id'] > 0) {
            $sim = $this::where([
                        ['id', '=', $data['id']]
                    ])->first();
            if (!$sim) {
                $sim = $this;
            }
        } else {
            $sim = $this;
        }
        
        $sim->sim_type = $data['sim_type'];
        $sim->sim_iccid = $data['sim_iccid'];
        $sim->sim_id  = $data['sim_id'];
        $sim->sim_no = $data['sim_no'];
        $sim->sim_status = $data['sim_status'];
        $sim->start_date = $data["start_date"];
        $sim->end_date = $data["end_date"];

        try {
            $sim->save();
            $jdata = json_encode(array('status' => 'success'));
            return $jdata;
        } catch (Exception $e) {
            //dd($e->getMessage());
            $jdata = json_encode(array('status' => 'failed'));
            return $jdata;
        }
    }

     public function deleteSim($delids) {

        foreach ($delids as $id) {
            $sim = $this->find($id);
            $sim = $this::where([
                        ['id', '=', $id]
                    ])->delete();
        }
        return 1;
    }
}
