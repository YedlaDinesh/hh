<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Accessorie extends Model
{
    //
    public function saveAccessorie($data) {

        $id = $data['id'];
        //die($id);
        if ($data['id'] > 0) {
            $accessorie = $this::where([
                        ['id', '=', $data['id']]
                    ])->first();
            if (!$accessorie) {
                $accessorie = $this;
            }
        } else {
            $accessorie = $this;
        }
        
        $accessorie->accessorie_name = $data['accessorie_name'];
        $accessorie->description = $data['description'];
        $accessorie->price = $data['price'];
        try {
            $accessorie->save();
            $jdata = json_encode(array('status' => 'success'));
            return $jdata;
        } catch (Exception $e) {
            //dd($e->getMessage());
            $jdata = json_encode(array('status' => 'failed'));
            return $jdata;
        }
    }

     public function deleteAccessorie($delids) {

        foreach ($delids as $id) {
            $accessorie = $this->find($id);
            $accessorie = $this::where([
                        ['id', '=', $id]
                    ])->delete();
        }
        return 1;
    }
}
