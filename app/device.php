<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class device extends Model
{
    //
     public function saveDevice($data) {

        $id = $data['id'];
        //die($id);
        if ($data['id'] > 0) {
            $device = $this::where([
                        ['id', '=', $data['id']]
                    ])->first();
            if (!$device) {
                $device = $this;
            }
        } else {
            $device = $this;
        }
        
        $device->device_name = $data['device_name'];
        $device->device_imei = $data['device_imei'];
        $device->device_id  = $data['device_id'];
        $device->app_loaded = $data['app_loaded'];
        $device->device_type = $data['device_type'];
        
        try {
            $device->save();
            $jdata = json_encode(array('status' => 'success'));
            return $jdata;
        } catch (Exception $e) {
            //dd($e->getMessage());
            $jdata = json_encode(array('status' => 'failed'));
            return $jdata;
        }
	}

	public function deleteDevice($delids) {

        foreach ($delids as $id) {
            $device = $this->find($id);
            $device = $this::where([
                        ['id', '=', $id]
                    ])->delete();
        }
        return 1;
    }
}
