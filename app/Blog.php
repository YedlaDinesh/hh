<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class Blog extends Model
{
    use Sluggable;
    //
     public function saveBlog($data) {

        $id = $data['id'];
        //die($id);
        if ($data['id'] > 0) {
            $blog = $this::where([
                        ['id', '=', $data['id']]
                    ])->first();
            if (!$blog) {
                $blog = $this;
                print_r($blog->image);
                die;
            }
        } else {
            $blog = $this;
        }

        $blog->title = $data['title'];
        $blog->description = $data['description'];
        $blog->slug = $data['slug'];
        $blog->is_published = $data['is_published'];
        $blog->cat_id = $data['cat_id'];
        $blog->image = (!empty($data['image'])) ? $data['image'] : $blog->image;

        try {
            $blog->save();
            $jdata = json_encode(array('status' => 'success'));
            return $jdata;
        } catch (Exception $e) {
            //dd($e->getMessage());
            $jdata = json_encode(array('status' => 'failed'));
            return $jdata;
        }
	}

	public function deleteBlog($delids) {

        foreach ($delids as $id) {
            $blog = $this->find($id);

            $exists =  public_path().'/images/blogs/thumbnail/'. $blog->image;
            $existsThumbnail =  public_path().'/images/blogs/'. $blog->image;
            if (File::exists($exists) || File::exists($existsThumbnail)) {
                File::delete(public_path('images/blogs' . '/' . $blog->image));
                File::delete(public_path('images/blogs/thumbnail' . '/' . $blog->image));
            }

            $blog = $this::where([
                        ['id', '=', $id]
                    ])->delete();
        }
        return 1;
    }

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public function category()
    {
        return $this->belongsTo('App\Categorie', 'cat_id');
    }
}
