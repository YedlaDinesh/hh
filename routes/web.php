<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Chcek if user login
Route::group(['prefix' => 'admin'], function () {
  Auth::routes();
});

Route::group(['middleware' => ['auth'], 'prefix' => 'admin'], function () {
  Route::get('/', 'HomeController@checkLogin');
  Route::get('/home', 'HomeController@index')->name('home');
  //Devices
  Route::get('/device/list', 'DeviceController@index');
  Route::get('/device/addedit/{id}', 'DeviceController@addedit');
  Route::post('/device/addedit/{id}', 'DeviceController@store');
  Route::post('/device/listaction', 'DeviceController@listaction');
  //Sim
  Route::get('/sim/list', 'SimController@index');
  Route::get('/sim/addedit/{id}', 'SimController@addedit');
  Route::post('/sim/addedit/{id}', 'SimController@store');
  Route::post('/sim/listaction', 'SimController@listaction');
  //Accessories
  Route::get('/accessorie/list', 'AccessorieController@index');
  Route::get('/accessorie/addedit/{id}', 'AccessorieController@addedit');
  Route::post('/accessorie/addedit/{id}', 'AccessorieController@store');
  Route::post('/accessorie/listaction', 'AccessorieController@listaction');
  //Plans
  Route::get('/planMobile/list', 'PlanController@listMobile');
  Route::get('/plan/addeditMobile/{id}', 'PlanController@addeditMobile');
  Route::post('/plan/addeditMobile/{id}', 'PlanController@storeMobile');

  Route::get('/planWifi/list', 'PlanController@listWifi');
  Route::get('/plan/addeditWifi/{id}', 'PlanController@addeditWifi');
  Route::post('/plan/addeditWifi/{id}', 'PlanController@storeWifi');
  //Category
  Route::get('/category/list', 'CategoryController@index');
  Route::get('/category/addedit/{id}', 'CategoryController@addedit');
  Route::post('/category/addedit/{id}', 'CategoryController@store');
  Route::post('/category/listaction', 'CategoryController@listaction');

  //Ticket
  Route::get('/ticket/list', 'TicketController@index');
  Route::get('/ticket/detail/{id}', 'TicketController@find');
  Route::post('/ticket/detail/{id}', 'TicketController@update');
  //blogs
  Route::get('/blog/list', 'BlogsController@index');
  Route::get('/blog/addedit/{id}', 'BlogsController@addedit');
  Route::post('/blog/addedit/{id}', 'BlogsController@store');
  Route::post('/blog/listaction', 'BlogsController@listaction');
  Route::get('/blog/check_slug', 'BlogsController@check_slug');

  //Faqs
  Route::get('/faq/list', 'FaqController@index');
  Route::get('/faq/addedit/{id}', 'FaqController@addedit');
  Route::post('/faq/addedit/{id}', 'FaqController@store');
  Route::post('/faq/listaction', 'FaqController@listaction');
  //Orders
  Route::get('/order/list', 'OrderController@index');
  Route::get('/order/detail/{id}', 'OrderController@find');
  Route::post('/order/detail/{id}', 'OrderController@update');
  //Customer
  Route::get('/customer/list', 'OrderController@getCustomer');
  Route::get('/order/detail/{id}', 'OrderController@find');
  Route::post('/order/detail/{id}', 'OrderController@update');

   //Leads
   Route::get('/leads/list', 'OrderController@listLeads');

  //Payments
  Route::get('/payments/list', 'OrderController@listPayments');


});

Route::get('/', 'BlogsController@home');

Route::get('/plan-pricing', 'PlanController@getPlans');
Route::get('/book-now', 'BooknowController@getDevice');

//Route::view('/login', 'home.login');
Route::get('/create-ticket', 'TicketController@viewPage');
Route::post('/create-ticket', 'TicketController@store');
Route::view('/refer-earn', 'home.refer-earn');
Route::view('/partner-with-us', 'home.partner-with-us');

Route::get('/blogs', 'BlogsController@showblog');
Route::get('/blog/{slug}', 'BlogsController@show');

// Display faqs
Route::get('/faq', 'FaqController@show');
Route::view('/reviews', 'home.reviews');

//ajax
Route::get('/getDevicePlan/{deviceId}', 'BooknowController@getDevicePlan');
Route::get('/getPlanPrice/{planId}', 'BooknowController@getPlanPrice');
Route::post('/bookDevice', 'BooknowController@bookDevice');
Route::get('/procees-page/{id}', 'BooknowController@processPage');
Route::post('/procees-page/{id}', 'BooknowController@processPageUpload');

Route::get('/paymentSuccess/{id}', 'BooknowController@paymentSuccess');

Route::get('/login', 'BooknowController@login');
Route::post('/login', 'BooknowController@loginCheck');

